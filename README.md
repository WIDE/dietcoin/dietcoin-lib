### dietcoin-lib

dietcoin-lib is a fork of bitcoinj-0.14.4, this repository only contains the library code.
The programs using dietcoin-lib are in dietcoin-exp. Debug can be done using the examples sub-project as explained below.

To list the modifications made since the fork of bitcoinj-0.14.4:

    $ git diff --name-only v0.14.4 HEAD
    $ git diff v0.14.4 HEAD

#### Build

This repository requires java 8 and gradle:

    $ apt-get install openjdk-8-jdk openjdk-8-jre gradle maven

To remove what gradle built:

    $ gradle clean

To build the project and create a fat jar with all the dependencies:

    $ gradle shadowjar

#### Debug

Pre-configure the project for eclipse, then in eclipse File > Open projects from file system:

    $ gradle eclipse

The library is in core/ and you can use the class DietExample to debug the library:

    $ cd examples/
    $ gradle shadowjar
    $ java -jar build/libs/bitcoinj-examples*.jar

#### Switching back from gradle to maven

    $ find . -name 'pom.xml.bkp' -exec sh -c 'f={}; echo mv "${f}" "${f:0:-4}"' \;
    $ gradle clean
    $ mvn eclipse:clean eclipse:eclipse

----

[![Build Status](https://travis-ci.org/bitcoinj/bitcoinj.png?branch=master)](https://travis-ci.org/bitcoinj/bitcoinj)   [![Coverage Status](https://coveralls.io/repos/bitcoinj/bitcoinj/badge.png?branch=master)](https://coveralls.io/r/bitcoinj/bitcoinj?branch=master) 

[![Visit our IRC channel](https://kiwiirc.com/buttons/irc.freenode.net/bitcoinj.png)](https://kiwiirc.com/client/irc.freenode.net/bitcoinj)

### Welcome to bitcoinj

The bitcoinj library is a Java implementation of the Bitcoin protocol, which allows it to maintain a wallet and send/receive transactions without needing a local copy of Bitcoin Core. It comes with full documentation and some example apps showing how to use it.

### Technologies

* Java 6 for the core modules, Java 8 for everything else
* [Maven 3+](http://maven.apache.org) - for building the project
* [Orchid](https://github.com/subgraph/Orchid) - for secure communications over [TOR](https://www.torproject.org)
* [Google Protocol Buffers](https://github.com/google/protobuf) - for use with serialization and hardware communications

### Getting started

To get started, it is best to have the latest JDK and Maven installed. The HEAD of the `master` branch contains the latest development code and various production releases are provided on feature branches.

#### Building from the command line

To perform a full build use
```
mvn clean package
```
You can also run
```
mvn site:site
```
to generate a website with useful information like JavaDocs.

The outputs are under the `target` directory.

#### Building from an IDE

Alternatively, just import the project using your IDE. [IntelliJ](http://www.jetbrains.com/idea/download/) has Maven integration built-in and has a free Community Edition. Simply use `File | Import Project` and locate the `pom.xml` in the root of the cloned project source tree.

### Example applications

These are found in the `examples` module.

#### Forwarding service

This will download the block chain and eventually print a Bitcoin address that it has generated.

If you send coins to that address, it will forward them on to the address you specified.

```
  cd examples
  mvn exec:java -Dexec.mainClass=org.bitcoinj.examples.ForwardingService -Dexec.args="<insert a bitcoin address here>"
```

Note that this example app *does not use checkpointing*, so the initial chain sync will be pretty slow. You can make an app that starts up and does the initial sync much faster by including a checkpoints file; see the documentation for
more info on this technique.

### Where next?

Now you are ready to [follow the tutorial](https://bitcoinj.github.io/getting-started).
