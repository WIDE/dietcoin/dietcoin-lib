### Must do
* DietBlockChain: handle filtered blocks as well
* DietExample: problem of group not stopping when hit by a SIGINT, then resuming
* LevelDB: separate removing a shard and versioning it, same for tree nodes
* LevelDB: index versioned nodes in database by their deletion height to avoid storing all of them every time
* Peer: handle getdata for transactions (imply having a tx index)
* Peer: a full node must only relay txs that match an SPV filter
* BloomFilter: find why SPV nodes don't receive tx giving them money, look as spec
* Try with multiple clients
* Fix existing tests
* Add tests for added bitcoin features
* Add tests for dietcoin features
* Use more VarInt() in bitcoinSerializeToStream

### Maybe one day
* Handle filteradd for bloom filters (BIP 37)
* Support service tag NODE_BLOOM (BIP 111), there is already the PR 1212 of bitcoinj implementing it
