package dietcoin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import org.bitcoinj.core.Context;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.core.PartialMerkleTree;
import org.bitcoinj.core.ProtocolException;
import org.bitcoinj.core.Sha256Hash;
import org.bitcoinj.core.Utils;
import org.bitcoinj.store.BlockStoreException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The structure and protocol messages for sharing partial Merkle tree to prove the inclusion of transactions
 * in a block (merkleblock message) is reused to prove the inclusion of UTXO shards in the set of shards.
 * https://bitcoin.org/en/developer-reference#merkleblock
 *  **/
public class UTXOPartialMerkleTreeMessage extends PartialMerkleTree {
    public static final String NAME = "utxomtree";

    private static final Logger log = LoggerFactory.getLogger(UTXOPartialMerkleTreeMessage.class);

    private LevelDBShardedBlockStore store;

    /** Dummy constructor for the static methods */
    protected UTXOPartialMerkleTreeMessage() {
        super(Context.get().getParams(), null, null, 0);
        store = null;
    }

    /** Used when creating a tree to answer a getutxomtree query **/
    public UTXOPartialMerkleTreeMessage(NetworkParameters params, byte[] bits, List<Sha256Hash> hashes, int origTxCount) {
        super(params, bits, hashes, origTxCount);
        store = null;
    }

    public UTXOPartialMerkleTreeMessage(NetworkParameters params, byte[] payloadBytes, int offset)
            throws ProtocolException {
        super(params, payloadBytes, offset);
        store = null;
    }

    /** Used when receiving a message, works in pair with parse(). */
    public UTXOPartialMerkleTreeMessage(NetworkParameters params, byte[] payloadBytes) {
        super(params, payloadBytes, 0);
        store = null;
    }

    @Override
    protected void parse() throws ProtocolException {
        super.parse();
    }

    /** Used to update the database with a new Merkle tree when parsing a receiveng tree */
    @Override
    protected void storeNode(int height, int pos, Sha256Hash hash) {
        if (store == null) {
            return;
        }
        int heightTree = PrefixShardingPolicy.getKeyLengthInBit();
        int index = (int) Math.pow(2, heightTree - height) + pos - 1;
        try {
            store.putMerkleNode(index, hash);
        } catch (BlockStoreException e) {
            log.error("Error storing a Merkle node in database " + e);
        }
    }

    public void setStore(LevelDBShardedBlockStore store) {
        this.store = store;
    }


    // Helper class to update the tree, conveniently already written for another purpose
    protected class PairNode extends VersionedUTXOShardIndex {
        public PairNode(int index, UTXOShardKey shardKey) {
            super(index, shardKey);
        }
        public int getIndex() { return super.getDeletionHeight(); };
    }

    /** Update the MTree stored in database and version the modified nodes with the height of the current block */
    public static void updateTreeDatabase(LevelDBShardedBlockStore store, int blockHeight, Sha256Hash blockHash)
            throws BlockStoreException {
        /**
         * Algo:
         * for all the modified leaves and their parents, starting from the leaves:
         * - version the node
         * - update the node value based on (i) children's values, or (ii) new shard hash if the node is a leaf
         * - enqueue the node's parent
         */

        // Dummy object to be allowed to use the PairNode class
        UTXOPartialMerkleTreeMessage dummy = new UTXOPartialMerkleTreeMessage();

        // Get the tree leaf index of all the shards that were modified by this block
        Queue<PairNode> nodes = new LinkedList<PairNode>();
        HashSet<Integer> modifiedNodes = new HashSet<Integer>();
        for (UTXOShardKey shardKey : PrefixShardingPolicy.getAllKeysFor(blockHash, store)) {
            int index = store.getMerkleLeafIndex(shardKey);
            nodes.add(dummy.new PairNode(index, shardKey));
            modifiedNodes.add(index);
        }

        while (!nodes.isEmpty()) {
            PairNode node = nodes.poll();

            // Compute the node new hash
            Sha256Hash newValue = null;
            if (node.getShardKey() == null) {
                // Non-leaf: update the node with its children's hashes (in a complete tree)
                Sha256Hash hashLeft = store.getMerkleNode(2 * node.getIndex() + 1);
                if (hashLeft == null) {
                    hashLeft = Sha256Hash.ZERO_HASH;
                }
                Sha256Hash hashRight = store.getMerkleNode(2 * node.getIndex() + 2);
                if (hashRight == null) {
                    hashRight = Sha256Hash.ZERO_HASH;
                }
                newValue = PartialMerkleTree.combineLeftRight(hashLeft.getBytes(), hashRight.getBytes());
            } else {
                // Leaf: update the node with the shard hash
                newValue = Sha256Hash.ZERO_HASH;
                UTXOShard shard = store.getUTXOShard(node.getShardKey());
                if (shard != null) {
                    newValue = shard.getHash();
                }
            }

            // Update node's value and version its old value if there is one
            Sha256Hash oldValue = store.getMerkleNode(node.getIndex());
            if (oldValue != null) {
                store.putVersionedMerkleNode(new VersionedMerkleTreeNodeIndex(blockHeight, node.getIndex()), oldValue);
                store.removeMerkleNode(node.getIndex());
            }
            store.putMerkleNode(node.getIndex(), newValue);

            // Reached the root of the tree
            if (node.getIndex() == 0) {
                break;
            }
            // Enqueue the parent node
            int parentIndex = node.getIndex() / 2;
            if (node.getIndex() % 2 == 0) {
                parentIndex--;
            }
            // Avoid processing the same node twice
            if (!modifiedNodes.contains(parentIndex)) {
                modifiedNodes.add(parentIndex);
                nodes.add(dummy.new PairNode(parentIndex, null));
            }
        }
    }

    // Helper class to build a partial Merkle tree based on UTXO shards
    protected class MerkleNode {
        int index;
        Sha256Hash hash;
        boolean flag;
        protected MerkleNode(int index, Sha256Hash hash, boolean flag) {
            this.index = index;
            this.hash = hash;
            this.flag = flag;
        }
    }

    /**
     * Build a full-fledged Merkle tree based on UTXO shards given as parameters.
     * Searches through the versioned Merkle nodes to find the ones corresponding to the pair (shards, queryDate).
     * There are 2^x shards, which results in a complete tree, so there is no need to worry about tree nodes not
     * having siblings, so no need to hash such node twice as the doc says.
     * */
    public static UTXOPartialMerkleTreeMessage buildFromShards(LevelDBShardedBlockStore store,
            Collection<UTXOShard> shards, Sha256Hash queryDate) throws BlockStoreException {
        /**
         * Algo:
         * 1- From the shards, get their keys, hashes and index in the tree
         * 2- Find all the nodes in tree that must be used in the partial tree, start from the leaves and parse up
         *    https://bitcoin.org/en/developer-reference#merkleblock
         * 3- Build the partial tree from the root down by polling the set of nodes that must be put in the tree
         */

        // Dummy object to be allowed to use the MerkleNode class
        UTXOPartialMerkleTreeMessage dummy = new UTXOPartialMerkleTreeMessage();

        // Convert the shards into Merkle tree leaves
        List<MerkleNode> leaves = new LinkedList<MerkleNode>();
        for (UTXOShard shard : shards) {
            int index = store.getMerkleLeafIndex(shard.getKey());
            leaves.add(dummy.new MerkleNode(index, shard.getHash(), true));
        }

        // Map<index of the node in the tree, node data to reply>
        Map<Integer, MerkleNode> nodes = new HashMap<Integer, MerkleNode>();
        for (MerkleNode leaf: leaves) {
            MerkleNode node = leaf;
            nodes.put(node.index, node);

            while (node.index != 0) {
                int siblingIndex = node.index + 1;
                int parentIndex = (node.index - 1) / 2;
                if (node.index % 2 == 0) {
                    // Current node is the right sibling
                    siblingIndex = node.index - 1;
                    parentIndex = node.index / 2 - 1;
                }

                // Add sibling node in the tree set, make sure not to erase a previous node set with a 1 flag
                if (!nodes.containsKey(siblingIndex)) {
                    Sha256Hash siblingHash = store.getMostFittingMerkleNode(siblingIndex, queryDate);
                    if (siblingHash == null) {
                        throw new BlockStoreException("Cannot find Merkle node with index: " + siblingIndex +
                                ", and queryDate: " + queryDate);                    }
                    MerkleNode siblingNode = dummy.new MerkleNode(siblingIndex, siblingHash, false);
                    nodes.put(siblingIndex, siblingNode);
                }

                // Only update parent node if it's already present in the set with its flag unset
                if (nodes.containsKey(parentIndex)) {
                    MerkleNode parentNode = nodes.get(parentIndex);
                    if (!parentNode.flag) {
                        parentNode.flag = true;
                        nodes.put(parentIndex, parentNode); // TODO DTC: overkill?
                    }
                    // The parent branch has already been treated
                    break;
                }

                // First time treating the parent node, add it to the set and iterate the loop with it
                Sha256Hash parentHash = store.getMostFittingMerkleNode(parentIndex, queryDate);
                if (parentHash == null) {
                    throw new BlockStoreException("Cannot find Merkle node with index: " + parentIndex +
                            ", and queryDate: " + queryDate);
                }
                MerkleNode parentNode = dummy.new MerkleNode(parentIndex, parentHash, true);
                nodes.put(parentIndex, parentNode);

                node = parentNode;
            }
        }

        // Parse the tree from root to leaves and construct the final tree
        int smallestLeafIndex = (int) Math.pow(2, PrefixShardingPolicy.getKeyLengthInBit()) - 1;
        List<Sha256Hash> pmtHashes = new ArrayList<Sha256Hash>();
        byte[] pmtFlags = new byte[(int) Math.ceil(nodes.size() / 8.0)];
        int bitCounter = 0;
        Deque<Integer> stack = new LinkedList<Integer>();
        stack.addFirst(0); // root
        while (!stack.isEmpty()) {
            int index = stack.removeFirst();
            MerkleNode node = nodes.get(index);
            if (node == null) {
                // Not a node of interest
                continue;
            }

            // Push hash if the node is a leaf or not a leaf and flag set to false
            if (node.index >= smallestLeafIndex || (node.index < smallestLeafIndex && !node.flag)) {
                pmtHashes.add(node.hash);
            }

            if (node.flag) {
                Utils.setBitLE(pmtFlags, bitCounter);
            }

            stack.addFirst(node.index * 2 + 2); // right branch
            stack.addFirst(node.index * 2 + 1); // left branch
            bitCounter++;
        }

        int sizeLeafset = (int) Math.pow(2, PrefixShardingPolicy.getKeyLengthInBit());
        return new UTXOPartialMerkleTreeMessage(Context.get().getParams(), pmtFlags, pmtHashes, sizeLeafset);
    }
}
