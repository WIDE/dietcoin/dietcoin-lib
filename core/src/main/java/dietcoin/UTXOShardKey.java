package dietcoin;

import static com.google.common.base.Preconditions.checkArgument;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

import org.bitcoinj.core.Sha256Hash;
import org.bitcoinj.core.Utils;

/** Container for a byte array. It's clearer than having byte[] everywhere. **/
public class UTXOShardKey {

    private byte[] key;

    private UTXOShardKey(byte[] shardKey) {
        this.key = shardKey;
    }

    public static UTXOShardKey wrap(byte[] shardKey) {
        checkArgument(shardKey.length == Sha256Hash.LENGTH);
        return new UTXOShardKey(shardKey);
    }

    public static UTXOShardKey copyOf(byte[] shardKey) {
        checkArgument(shardKey.length == Sha256Hash.LENGTH);
        return new UTXOShardKey(Arrays.copyOf(shardKey, shardKey.length));
    }

    public static UTXOShardKey fromStream(InputStream in) throws IOException {
        byte[] bytes = new byte[Sha256Hash.LENGTH];
        if (in.read(bytes) != Sha256Hash.LENGTH)
            throw new EOFException();
        return UTXOShardKey.wrap(bytes);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        UTXOShardKey cast = (UTXOShardKey) obj;
        return Arrays.equals(getBytes(), cast.getBytes());
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(getBytes());
    }

    @Override
    public String toString() {
        return "" + Utils.readUint32(getBytes(), 0);
    }

    public byte[] getBytes() {
        return key;
    }
}
