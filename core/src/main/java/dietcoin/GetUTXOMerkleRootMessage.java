package dietcoin;

import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.core.Sha256Hash;

public class GetUTXOMerkleRootMessage extends SingleHashMessage {
    public static final String NAME = "getutxomroot";

    /** Used when sending a message, works in pair with bitcoinSerializeToStream(). */
    public GetUTXOMerkleRootMessage(NetworkParameters params, Sha256Hash blockHash) {
        super(params, blockHash);
    }

    /** Used when receiving a message, works in pair with parse(). */
    public GetUTXOMerkleRootMessage(NetworkParameters params, byte[] payloadBytes) {
        super(params, payloadBytes);
    }

    @Override
    public String getMessageName() {
        return NAME;
    }
}
