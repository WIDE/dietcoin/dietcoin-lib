package dietcoin;

import java.io.IOException;
import java.io.OutputStream;

import org.bitcoinj.core.Message;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.core.ProtocolException;
import org.bitcoinj.core.Sha256Hash;

abstract public class SingleHashMessage extends Message {
    // Not calling it hash because of Message.getHash() behavior and comment
    private Sha256Hash contentHash;

    /** Used when sending a message, works in pair with bitcoinSerializeToStream(). */
    public SingleHashMessage(NetworkParameters params, Sha256Hash contentHash) {
        super(params);
        this.contentHash = contentHash;
    }

    @Override
    protected void bitcoinSerializeToStream(OutputStream stream) throws IOException {
        stream.write(contentHash.getReversedBytes());
    }

    /** Used when receiving a message, works in pair with parse(). */
    public SingleHashMessage(NetworkParameters params, byte[] payloadBytes) {
        super(params, payloadBytes, 0);
    }

    @Override
    protected void parse() throws ProtocolException {
        contentHash = readHash();
        length = cursor;
    }

    @Override
    public String toString() {
        return "message " + getMessageName() + " with hash: " + getBlockHash();
    }

    /**
     * Get the hash contained in the message.
     */
    public Sha256Hash getContentHash() {
        return contentHash;
    }
    public Sha256Hash getBlockHash() {
        return getContentHash();
    }

    abstract public String getMessageName();
}
