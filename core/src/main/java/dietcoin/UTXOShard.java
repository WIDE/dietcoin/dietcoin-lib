package dietcoin;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Locale;
import java.util.Objects;
import java.util.TreeSet;

import org.bitcoinj.core.Context;
import org.bitcoinj.core.Message;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.core.Sha256Hash;
import org.bitcoinj.core.TransactionOutPoint;
import org.bitcoinj.core.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UTXOShard extends Message {
    public static final int LENGTH_REQUEST = 32 + 32; // Only key and creationBlockHash

    private static final Logger log = LoggerFactory.getLogger(UTXOShard.class);

    private UTXOShardKey key;
    private Sha256Hash creationBlockHash;
    private int creationBlockHeight;
    private TreeSet<TransactionOutPoint> outpoints;
    private Sha256Hash hash;

    // TODO DTC: only recompute the hash when needed

    /** Used to create a shard query when sending/receiving it. */
    public UTXOShard(UTXOShardKey key, Sha256Hash creationBlockHash) {
        this(key, creationBlockHash, -1, new TreeSet<TransactionOutPoint>());
    }

    @Override
    protected void bitcoinSerializeToStream(OutputStream stream) throws IOException {
        stream.write(Utils.reverseBytes(getKey().getBytes()));
        stream.write(getCreationBlockHash().getReversedBytes());
        Utils.uint32ToByteStreamLE(getCreationBlockHeight(), stream);
        Utils.uint32ToByteStreamLE(outpoints.size(), stream);
        for (TransactionOutPoint outpoint : outpoints) {
            outpoint.bitcoinSerialize(stream);
        }
    }

    /** Used when receiving a shard, works in pair with parse(). */
    public UTXOShard(NetworkParameters params, byte[] payloadBytes) {
        super(params, payloadBytes, 0);
    }

    public UTXOShard(NetworkParameters params, byte[] payloadBytes, int offset) {
        super(params, payloadBytes, offset);
    }

    @Override
    protected void parse() {
        // Read the metadata of a shard
        // - 32B key of shard
        // - 32B block creation hash of shard
        // - 4B block creation height of shard
        // - 4B number of outpoints
        //     - 32B hash outpoint #1
        //     - 4B  index outpoint #1
        //     ...
        int initialCursor = cursor;
        key = UTXOShardKey.wrap(Utils.reverseBytes(readBytes(Sha256Hash.LENGTH)));
        creationBlockHash = readHash();
        creationBlockHeight = (int) readUint32();
        int nbOutpoints = (int) readUint32();
        outpoints = new TreeSet<TransactionOutPoint>();
        for (int i = 0; i < nbOutpoints; i ++) {
            TransactionOutPoint outpoint = new TransactionOutPoint(Context.get().getParams(), payload, cursor);
            outpoints.add(outpoint);
            cursor += outpoint.getMessageSize();
        }
        length = cursor - initialCursor;
        hash = computeHash();
    }

    /** Used to create an empty shard to fill later. */
    public UTXOShard(UTXOShardKey key, Sha256Hash creationBlockHash, int creationBlockHeight) {
        this(key, creationBlockHash, creationBlockHeight, new TreeSet<TransactionOutPoint>());
    }

    /** Copy constructor, watch out for the Collections content that don't get copied */
    public UTXOShard(UTXOShard dolly) {
        this.key = dolly.key;
        this.creationBlockHash = dolly.creationBlockHash;
        this.creationBlockHeight = dolly.creationBlockHeight;
        this.outpoints = new TreeSet<TransactionOutPoint>();
        for (TransactionOutPoint outpoint : dolly.outpoints) {
            this.outpoints.add(new TransactionOutPoint(Context.get().getParams(), outpoint.getIndex(), outpoint.getHash()));
        }
        this.hash = dolly.hash;
    }

    private UTXOShard(UTXOShardKey key, Sha256Hash creationBlockHash, int creationBlockHeight, TreeSet<TransactionOutPoint> outpoints) {
        this.key = key;
        this.creationBlockHash = creationBlockHash;
        this.creationBlockHeight = creationBlockHeight;
        this.outpoints = outpoints;
        this.hash = computeHash();
    }

    /** Used for database serializing, works in pair with serializeToStream(). */
    public UTXOShard(NetworkParameters params, InputStream in) throws IOException {
        this.hash = Sha256Hash.fromStream(in);
        this.key = UTXOShardKey.fromStream(in);
        this.creationBlockHash = Sha256Hash.fromStream(in);
        this.creationBlockHeight = (int) Utils.readUint32FromByteStreamLE(in);

        int size = (int) Utils.readUint32FromByteStreamLE(in);
        this.outpoints = new TreeSet<TransactionOutPoint>();
        for (int i = 0; i < size; i++) {
            this.outpoints.add(new TransactionOutPoint(params, in));
        }
    }

    public void serializeToStream(OutputStream out) throws IOException {
        out.write(getHash().getBytes());
        serializeToStreamWithoutHash(out);
    }

    private void serializeToStreamWithoutHash(OutputStream out) throws IOException {
        out.write(getKey().getBytes());
        out.write(creationBlockHash.getBytes());
        Utils.uint32ToByteStreamLE(creationBlockHeight, out);

        Utils.uint32ToByteStreamLE(outpoints.size(), out);
        for (TransactionOutPoint outpoint : outpoints) {
            outpoint.serializeToStream(out);
        }
    }

    public Sha256Hash getHash() {
        updateHash();
        return hash;
    }

    /** Computes the shard hash only when needed. */
    private void updateHash() {
        hash = computeHash();
    }

    /**
     * Computes the hash of the serialized shard.
     * hash = sha256(sha256( serialized(key, creation hash, creation height, utxo outpoints) ))
     * @return The computed hash
     * @throws IOException
     */
    private Sha256Hash computeHash() {
        Sha256Hash hash = null;
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            serializeToStreamWithoutHash(bos);
            hash = Sha256Hash.twiceOf(bos.toByteArray());
        } catch (IOException e) {
            log.error("Error computing shard hash of key " + getKey().getBytes() + ": ", e);
        }
        return hash;
    }

    /** Two shards are equal if they have the same key and are generated from the same block hash (creation date). */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        UTXOShard cast = (UTXOShard) obj;
        return creationBlockHash.equals(cast.creationBlockHash) && getKey().equals(cast.getKey());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCreationBlockHash(), getKey());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("UTXO shard");
        Long keyHumanReadable = Utils.readUint32(getKey().getBytes(), 0);
        sb.append("\tkey: ").append(keyHumanReadable);
        sb.append("\thash: ").append(getHash());
        sb.append("\toutpoints: ").append(getOutPoints().size());

        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            for (TransactionOutPoint outpoint : getOutPoints()) {
                outpoint.serializeToStream(bos);
            }
            double size = bos.toByteArray().length / 1024.0;
            sb.append("\tsize: ").append(String.format(Locale.US, "%.3f", size)).append(" KiB");
        } catch (IOException e) {
            log.error("Error serializing a UTXO shard, key: " + getKey().getBytes(), e);
        }
        sb.append("\tcreation height: ").append(getCreationBlockHeight());
        return sb.toString();
    }

    public UTXOShardKey getKey() {
        return key;
    }

    public Sha256Hash getCreationBlockHash() {
        return creationBlockHash;
    }

    public int getCreationBlockHeight() {
        return creationBlockHeight;
    }

    /** Used when versioning shards to update the current shard creation date without creating a new shard. */
    public void updateCreationDate(Sha256Hash creationBlockHash, int creationBlockHeight) {
        this.creationBlockHash = creationBlockHash;
        this.creationBlockHeight = creationBlockHeight;
    }

    public TreeSet<TransactionOutPoint> getOutPoints() {
        return outpoints;
    }

    public boolean containsOutPoint(Sha256Hash hash, long index) {
        return outpoints.contains(new TransactionOutPoint(Context.get().getParams(), index, hash));
    }

    public boolean addOutPoint(Sha256Hash hash, long index) {
        return outpoints.add(new TransactionOutPoint(Context.get().getParams(), index, hash));
    }

    public boolean removeOutPoint(Sha256Hash hash, long index) {
        return outpoints.remove(new TransactionOutPoint(Context.get().getParams(), index, hash));
    }
}
