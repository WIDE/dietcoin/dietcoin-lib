package dietcoin;

import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.core.Sha256Hash;

public class GetUTXOPartialMerkleTreeMessage extends SingleHashMessage {
    public static final String NAME = "getutxomtree";

    /** Used when sending a message, works in pair with bitcoinSerializeToStream(). */
    public GetUTXOPartialMerkleTreeMessage(NetworkParameters params, Sha256Hash blockHash) {
        super(params, blockHash);
    }

    /** Used when receiving a message, works in pair with parse(). */
    public GetUTXOPartialMerkleTreeMessage(NetworkParameters params, byte[] payloadBytes) {
        super(params, payloadBytes);
    }

    @Override
    public String getMessageName() {
        return NAME;
    }
}
