package dietcoin;

import java.util.Arrays;

/** Container for a byte array. It's clearer than having byte[] everywhere. **/
public class DatabaseKey {

    private byte[] key;

    private DatabaseKey(byte[] shardKey) {
        this.key = shardKey;
    }

    public static DatabaseKey wrap(byte[] dbKey) {
        return new DatabaseKey(dbKey);
    }

    public static DatabaseKey copyOf(byte[] dbKey) {
        return new DatabaseKey(Arrays.copyOf(dbKey, dbKey.length));
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        DatabaseKey cast = (DatabaseKey) obj;
        return Arrays.equals(getBytes(), cast.getBytes());
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(getBytes());
    }

    public byte[] getBytes() {
        return key;
    }
}
