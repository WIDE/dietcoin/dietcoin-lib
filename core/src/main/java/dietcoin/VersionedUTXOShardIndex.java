package dietcoin;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Objects;

import org.bitcoinj.core.Utils;

/**
 * This class contains the metadata needed to store and cleanup older versions of UTXO shards
 */
public class VersionedUTXOShardIndex {
    private int deletionHeight;
    private UTXOShardKey shardKey;

    public VersionedUTXOShardIndex(int deletionHeight, UTXOShardKey shardKey) {
        this.deletionHeight = deletionHeight;
        this.shardKey = shardKey;
    }

    public VersionedUTXOShardIndex(InputStream in) throws IOException {
        this.deletionHeight = (int) Utils.readUint32FromByteStreamLE(in);
        this.shardKey = UTXOShardKey.fromStream(in);
    }

    public void serializeToStream(OutputStream out) throws IOException {
        Utils.uint32ToByteStreamLE(deletionHeight, out);
        out.write(getShardKey().getBytes());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VersionedUTXOShardIndex other = (VersionedUTXOShardIndex) o;
        return deletionHeight == other.deletionHeight && shardKey.equals(other.shardKey);
    }

    @Override
    public int hashCode() {
        return Objects.hash(deletionHeight, shardKey);
    }

    public int getDeletionHeight() {
        return deletionHeight;
    }

    public UTXOShardKey getShardKey() {
        return shardKey;
    }
}
