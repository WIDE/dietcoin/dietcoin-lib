package dietcoin;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Objects;

import org.bitcoinj.core.Utils;

/**
 * This class contains the metadata needed to store older versions of nodes of the UTXO Merkle tree.
 */
public class VersionedMerkleTreeNodeIndex {
    private int deletionHeight;
    private int index;

    public VersionedMerkleTreeNodeIndex(int deletionHeight, int index) {
        this.deletionHeight = deletionHeight;
        this.index = index;
    }

    /** Used for database serializing, works in pair with serializeToStream(). */
    public VersionedMerkleTreeNodeIndex(InputStream in) throws IOException {
        this.deletionHeight = (int) Utils.readUint32FromByteStreamLE(in);
        this.index = (int) Utils.readUint32FromByteStreamLE(in);
    }

    public void serializeToStream(OutputStream out) throws IOException {
        Utils.uint32ToByteStreamLE(deletionHeight, out);
        Utils.uint32ToByteStreamLE(index, out);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VersionedMerkleTreeNodeIndex other = (VersionedMerkleTreeNodeIndex) o;
        return deletionHeight == other.deletionHeight && index == other.index;
    }

    @Override
    public int hashCode() {
        return Objects.hash(deletionHeight, index);
    }

    public int getDeletionHeight() {
        return deletionHeight;
    }

    public int getIndex() {
        return index;
    }
}
