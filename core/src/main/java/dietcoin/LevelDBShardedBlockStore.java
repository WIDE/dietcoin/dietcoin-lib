package dietcoin;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.bitcoinj.core.Address;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.core.Sha256Hash;
import org.bitcoinj.core.StoredBlock;
import org.bitcoinj.core.StoredUndoableBlock;
import org.bitcoinj.core.TransactionOutPoint;
import org.bitcoinj.core.UTXO;
import org.bitcoinj.core.UTXOProviderException;
import org.bitcoinj.core.Utils;
import org.bitcoinj.store.BlockStoreException;
import org.bitcoinj.store.LevelDBFullPrunedBlockStore;
import org.iq80.leveldb.DBException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LevelDBShardedBlockStore extends LevelDBFullPrunedBlockStore {
    // LRUCache
    protected class LRUCache<E> extends LinkedHashMap<DatabaseKey, E> {
        private static final long serialVersionUID = 1L;
        private int capacity;

        public LRUCache(int capacity, float loadFactor) {
            super(capacity, loadFactor, true);
            this.capacity = capacity;
        }

        @Override
        protected boolean removeEldestEntry(Map.Entry<DatabaseKey, E> eldest) {
            return size() > this.capacity;
        }
    }

    // Copied from LevelDBFullPrunedBlockStore.KeyType, fastest way to inherit an enum
    protected enum KeyTypeExtended {
        CREATED, CHAIN_HEAD_SETTING, VERIFIED_CHAIN_HEAD_SETTING, VERSION_SETTING, HEADERS_ALL, UNDOABLEBLOCKS_ALL, HEIGHT_UNDOABLEBLOCKS, OPENOUT_ALL, ADDRESS_HASHINDEX,
        /** Dietcoin specific */
        UTXO_SHARDING_POLICY,
        UTXO_SHARDS_CURRENT, UTXO_SHARDS_VERSIONED, UTXO_SHARDS_HISTORY, UTXO_VERSIONED,
        UTXO_MERKLE_TREE_CURRENT, UTXO_MERKLE_TREE_VERSIONED,
        UTXO_MERKLE_ROOT_COMMITMENTS, UTXO_MERKLE_ROOT_PENDING_COMMITMENTS, HEIGHT_OLDEST_MERKLE_ROOT_COMMITMENT
    }

    protected static final int DB_KEY_LENGTH_ONE_INT_ONE_HASH = 1 + Sha256Hash.LENGTH + 4; // 1 = enum ordinal
    protected static final int DB_KEY_LENGTH_ONE_INT = 1 + 4;
    protected static final int DB_KEY_LENGTH_ONE_HASH = 1 + Sha256Hash.LENGTH;
    protected static final int DB_KEY_LENGTH_ONE_HASH_ONE_LONG = 1 + Sha256Hash.LENGTH + 8;

    // Max cache size for the Merkle tree, it stores the top X layers (e.g., X = 15 => 1 MiB of cache)
    private static final int MERKLE_NODES_MAX_CACHE_SIZE = (int) Math.pow(2, 15) - 1;

    private static final Logger log = LoggerFactory.getLogger(LevelDBShardedBlockStore.class);

    // Versioning for shards/MTree nodes/committed MRoots, keep the deleted ones from the last X blocks to send to diet nodes
    protected int versioningMaxAge;

    // LRU cache for getUTXOShard()
    protected LRUCache<UTXOShard> utxoShardCache;
    // Additional buffers to temporarily store shards to be pushed to disk once the block is verified
    protected Map<DatabaseKey, UTXOShard> batchUTXOShards;
    protected Map<UTXOShardKey, UTXOShard> batchVersionedUTXOShards;
    protected Map<VersionedUTXOShardIndex, List<UTXO>> batchVersionedUTXOs;
    // Ordered queue of versioned shards (dbKey, deletion height), oldest shard first
    protected Deque<VersionedUTXOShardIndex> versionedShards;

    // Cache for the top of the MTree (i.e., the most modified nodes) and caches to complement leveldb batches
    protected int merkleNodesCacheSize;
    protected Map<Integer, Sha256Hash> merkleNodeCache;
    protected Map<Integer, Sha256Hash> merkleNodeUncommittedCache;
    protected Set<Integer> merkleNodeUncommittedDeletedCache;
    protected int heightOldestMerkleRootCommitment;

    // Ordered queue of versioned Merkle nodes with efficient get(), oldest node first
    protected LinkedHashMap<VersionedMerkleTreeNodeIndex, Sha256Hash> versionedMerkleNodes;


    public LevelDBShardedBlockStore(NetworkParameters params, String filename, int blockCount, int versioningMaxAge) {
        super(params, filename, blockCount);
        setVersioningMaxAge(versioningMaxAge);
    }

    public LevelDBShardedBlockStore(NetworkParameters params, String filename, int blockCount, int versioningMaxAge, long leveldbReadCache,
            int leveldbWriteCache, int openOutCache, boolean instrument, int exitBlock) {
        super(params, filename, blockCount, leveldbReadCache, leveldbWriteCache, openOutCache, instrument, exitBlock);
        setVersioningMaxAge(versioningMaxAge);
    }

    @Override
    protected void beginMethod(String name) {
        if (instrument) {
            super.beginMethod(name);
        }
    }

    @Override
    protected void endMethod(String name) {
        if (instrument) {
            super.endMethod(name);
        }
    }

    @Override
    protected void createNewStore(NetworkParameters params) throws BlockStoreException {
        super.createNewStore(params);
        versionedShards = new LinkedList<VersionedUTXOShardIndex>();
        versionedMerkleNodes = new LinkedHashMap<VersionedMerkleTreeNodeIndex, Sha256Hash>();
        merkleNodesCacheSize = 0;
        merkleNodeCache = null;
        heightOldestMerkleRootCommitment = Integer.MAX_VALUE;
    }

    @Override
    protected void openDB() {
        super.openDB();
        try {
            storeHeightOldestMerkleRootCommitment();
        } catch (BlockStoreException e) {
            log.error("Error while storing the height of the oldest Merkle root commitment: ", e);
        }
        utxoShardCache = new LRUCache<UTXOShard>(openOutCache, 0.75f);
    }

    @Override
    protected void initFromDb() throws BlockStoreException {
        super.initFromDb();
        readAndInitShardingPolicy();
        readAndInitShardsHistory();
        readAndInitVersionedMerkleNodes();
        heightOldestMerkleRootCommitment = readHeightOldestMerkleRootCommitment();
    }

    @Override
    public void resetStore() {
        super.resetStore();
        utxoShardCache = new LRUCache<UTXOShard>(openOutCache, 0.75f);
        resetMerkleNodesCaches(merkleNodesCacheSize);
    }

    // Only kept here to make sure everything needed is implemented
    @Override
    public void close() throws BlockStoreException {
        super.close();
    }

    @Override
    public void beginDatabaseBatchWrite() throws BlockStoreException {
        if (!autoCommit) {
            return;
        }
        // Super call must be put after the auto commit test
        super.beginDatabaseBatchWrite();
        beginMethod("beginDatabaseBatchWrite");
        try {
            batchUTXOShards = new HashMap<DatabaseKey, UTXOShard>();
            batchVersionedUTXOShards = new HashMap<UTXOShardKey, UTXOShard>();
            batchVersionedUTXOs = new HashMap<VersionedUTXOShardIndex, List<UTXO>>();
            merkleNodeUncommittedCache = new HashMap<Integer, Sha256Hash>();
            merkleNodeUncommittedDeletedCache = new HashSet<Integer>();
        } finally {
            endMethod("beginDatabaseBatchWrite");
        }
    }

    @Override
    public void commitDatabaseBatchWrite() throws BlockStoreException {
        if (batch == null) {
            return;
        }
        beginMethod("commitDatabaseBatchWrite");
        try {
            // Flush the batch
            db.write(batch);

            // Flush uncommitted merkle nodes into the main cache
            for (Map.Entry<Integer, Sha256Hash> entry : merkleNodeUncommittedCache.entrySet()) {
                merkleNodeCache.put(entry.getKey(), entry.getValue());
            }
            merkleNodeUncommittedCache = null;
            for (Integer entry : merkleNodeUncommittedDeletedCache) {
                merkleNodeCache.remove(entry);
            }
            merkleNodeUncommittedDeletedCache = null;

            // Temporarily disable uncommitted caches and desactivate batch writing
            autoCommit = true;

            try {
                batch.close();
                batch = null;
            } catch (IOException e) {
                log.error("Error in db commit.", e);
                throw new BlockStoreException("could not close batch.");
            }

            if (instrument && verifiedChainHeadBlock.getHeight() % 1000 == 0) {
                log.info("Height: " + verifiedChainHeadBlock.getHeight());
                dumpStats();
                if (verifiedChainHeadBlock.getHeight() == exitBlock) {
                    System.err.println("Exit due to exitBlock set");
                    System.exit(1);
                }
            }
        } finally {
            endMethod("commitDatabaseBatchWrite");
        }
    }

    @Override
    public void abortDatabaseBatchWrite() throws BlockStoreException {
        batchUTXOShards = null;
        batchVersionedUTXOShards = null;
        batchVersionedUTXOs = null;
        merkleNodeUncommittedCache = null;
        merkleNodeUncommittedDeletedCache = null;
        super.abortDatabaseBatchWrite();
    }

    protected void resetMerkleNodesCaches(int cacheSize) {
        if (cacheSize > MERKLE_NODES_MAX_CACHE_SIZE) {
            cacheSize = MERKLE_NODES_MAX_CACHE_SIZE;
        }
        merkleNodesCacheSize = cacheSize;
        merkleNodeCache = new HashMap<Integer, Sha256Hash>(merkleNodesCacheSize);
    }

    protected byte[] getKey(KeyTypeExtended keytype) {
        byte[] key = new byte[1];
        key[0] = (byte) keytype.ordinal();
        return key;
    }

    /** To speed up the replayer bootstrap, it can be set to 0 at first and then take a better value */
    public void setVersioningMaxAge(int versioningMaxAge) {
        this.versioningMaxAge = versioningMaxAge;
    }

    /**
     * Database access for cached data added in Dietcoin
     */

    public void readAndInitShardingPolicy() throws BlockStoreException {
        beginMethod("readAndInitShardingPolicy");
        try {
            byte[] policyBytes = batchGet(getKey(KeyTypeExtended.UTXO_SHARDING_POLICY));
            ByteArrayInputStream bis = new ByteArrayInputStream(policyBytes);
            PrefixShardingPolicy.deserializeFromStream(bis);

            // Create the Merkle nodes cache
            resetMerkleNodesCaches((int) Math.pow(2, PrefixShardingPolicy.getKeyLengthInBit() + 1) - 1);
        } catch (IOException e) {
            throw new BlockStoreException("Error loading sharding policy from database: " + e);
        } finally {
            endMethod("readAndInitShardingPolicy");
        }
    }

    public void setShardingPolicy(int keyLengthInBits) throws BlockStoreException {
        PrefixShardingPolicy.setKeyLengthInBit(keyLengthInBits);

        beginMethod("setShardingPolicy");
        try {
            // Store the sharding policy
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            PrefixShardingPolicy.serializeToStream(bos);
            batchPut(getKey(KeyTypeExtended.UTXO_SHARDING_POLICY), bos.toByteArray());

            // Reset the Merkle nodes cache whith its new size
            resetMerkleNodesCaches((int) Math.pow(2, PrefixShardingPolicy.getKeyLengthInBit() + 1) - 1);
        } catch (IOException e) {
            throw new BlockStoreException("Error storing sharding policy to database: " + e);
        } finally {
            endMethod("setShardingPolicy");
        }
    }

    public void readAndInitShardsHistory() throws BlockStoreException {
        beginMethod("readAndInitShardsHistory");
        try {
            byte[] historyBytes = batchGet(getKey(KeyTypeExtended.UTXO_SHARDS_HISTORY));
            ByteArrayInputStream bis = new ByteArrayInputStream(historyBytes);

            int arraySize = (int) Utils.readUint32FromByteStreamLE(bis);

            this.versionedShards = new LinkedList<VersionedUTXOShardIndex>();
            for (int i = 0; i < arraySize; i++) {
                versionedShards.add(new VersionedUTXOShardIndex(bis));
            }
        } catch (IOException e) {
            throw new BlockStoreException("Error loading UTXO shards history from database: " + e);
        } finally {
            endMethod("readAndInitShardsHistory");
        }
    }

    public void storeShardsHistory() throws BlockStoreException {
        beginMethod("storeShardsHistory");
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            Utils.uint32ToByteStreamLE(versionedShards.size(), bos);
            for (VersionedUTXOShardIndex shard : versionedShards) {
                shard.serializeToStream(bos);
            }
            batchPut(getKey(KeyTypeExtended.UTXO_SHARDS_HISTORY), bos.toByteArray());
        } catch (IOException e) {
            throw new BlockStoreException("Error storing UTXO shards history to database: " + e);
        } finally {
            endMethod("storeShardsHistory");
        }
    }

    public void readAndInitVersionedMerkleNodes() throws BlockStoreException {
        beginMethod("readAndInitVersionedMerkleNodes");
        try {
            byte[] dbBytes = batchGet(getKey(KeyTypeExtended.UTXO_MERKLE_TREE_VERSIONED));
            ByteArrayInputStream bis = new ByteArrayInputStream(dbBytes);

            int size = (int) Utils.readUint32FromByteStreamLE(bis);

            this.versionedMerkleNodes = new LinkedHashMap<VersionedMerkleTreeNodeIndex, Sha256Hash>();
            for (int i = 0; i < size; i++) {
                VersionedMerkleTreeNodeIndex nodeIndex = new VersionedMerkleTreeNodeIndex(bis);
                Sha256Hash hash = Sha256Hash.fromStream(bis);
                versionedMerkleNodes.put(nodeIndex, hash);
            }
        } catch (IOException e) {
            throw new BlockStoreException("Error loading UTXO Merkle tree nodes history from database: " + e);
        } finally {
            endMethod("readAndInitVersionedMerkleNodes");
        }
    }

    public void storeVersionedMerkleNodes() throws BlockStoreException {
        beginMethod("storeVersionedMerkleNodes");
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            Utils.uint32ToByteStreamLE(versionedMerkleNodes.size(), bos);
            for (VersionedMerkleTreeNodeIndex nodeIndex : versionedMerkleNodes.keySet()) {
                nodeIndex.serializeToStream(bos);
                Sha256Hash hash = versionedMerkleNodes.get(nodeIndex);
                bos.write(hash.getBytes());
            }
            batchPut(getKey(KeyTypeExtended.UTXO_MERKLE_TREE_VERSIONED), bos.toByteArray());
        } catch (IOException e) {
            throw new BlockStoreException("Error storing UTXO Merkle tree nodes history to database: " + e);
        } finally {
            endMethod("storeVersionedMerkleNodes");
        }
    }

    /**
     * Inherited methods to access the UTXOs
     */

    @Override
    public UTXO getTransactionOutput(Sha256Hash hash, long index) throws BlockStoreException {
        return super.getTransactionOutput(hash, index);
    }

    @Override
    public void addUnspentTransactionOutput(UTXO utxo, int blockHeight, Sha256Hash blockHash) throws BlockStoreException {
        // Find the shard the given UTXO should belong to and create a new shard if none is found
        UTXOShardKey shardKey = PrefixShardingPolicy.getKeyFor(utxo.getHash());
        UTXOShard shard = getUTXOShard(shardKey);
        if (shard == null) {
            shard = new UTXOShard(shardKey, getVerifiedChainHead().getHeader().getHash(), getVerifiedChainHead().getHeight());
        } else {
            // Version the previous state of the shard (if none found)
            if (getVersionedUTXOShard(shard.getKey(), blockHeight) == null) {
                UTXOShard vShard = new UTXOShard(shard);
                putVersionedUTXOShard(vShard, blockHeight, false);
            }
        }
        // Add UTXO and add to batch
        shard.addOutPoint(utxo.getHash(), utxo.getIndex());
        putUTXOShard(shard, false);

        // Store the UTXO in db/cache
        super.addUnspentTransactionOutput(utxo, blockHeight, blockHash);
    }

    @Override
    public void removeUnspentTransactionOutput(UTXO utxo, int blockHeight) throws BlockStoreException {
        // Find the shard the given UTXO should belong to
        UTXOShardKey shardKey = PrefixShardingPolicy.getKeyFor(utxo.getHash());
        UTXOShard shard = getUTXOShard(shardKey);
        if (shard == null) {
            log.error("Trying to remove UTXO " + utxo.getHash() + ":" + utxo.getIndex() + " from a non existing UTXO shard " + shardKey + " at block " + blockHeight);
            return;
        }

        if (!shard.containsOutPoint(utxo.getHash(), utxo.getIndex())) {
            // The UTXO is not in the set but it should be
            log.error("Trying to remove UTXO " + utxo.getHash() + ":" + utxo.getIndex() + " not stored in shard " + shardKey + " at block " + blockHeight);
            return;
        }

        // Version the previous state of the shard (if none found)
        if (getVersionedUTXOShard(shard.getKey(), blockHeight) == null) {
            UTXOShard vShard = new UTXOShard(shard);
            if (shard.getOutPoints().size() == 0)
            putVersionedUTXOShard(vShard, blockHeight, false);
        }
        // Store the new state with the removed UTXO or nothing if the shard is now empty
        shard.removeOutPoint(utxo.getHash(), utxo.getIndex());
        if (shard.getOutPoints().size() != 0) {
            putUTXOShard(shard, false);
        } else {
            removeUTXOShard(shard.getKey(), true); // write-through, I don't want another cache just for that line
        }

        // Remove the UTXO from db/cache
        super.removeUnspentTransactionOutput(utxo, blockHeight);
    }

    @Override
    public boolean hasUnspentOutputs(Sha256Hash hash, int numOutputs) throws BlockStoreException {
        return super.hasUnspentOutputs(hash, numOutputs);
    }

    // Only kept here to make sure everything needed is implemented
    @Override
    public List<UTXO> getOpenTransactionOutputs(List<Address> addresses) throws UTXOProviderException {
        beginMethod("getOpenTransactionOutputs");
        try {
            return super.getOpenTransactionOutputs(addresses);
        } finally {
            endMethod("getOpenTransactionOutputs");
        }
    }

    /**
     * Inherited other methods
     */

    @Override
    public void put(StoredBlock block) throws BlockStoreException {
        super.put(block);
        // The blockHash to height link is now known, so index the commitment by its block height
        tryCommitPendingMerkleRootCommitment(block.getHeight(), block.getHeader().getHash());
    }

    @Override
    public void put(StoredBlock storedBlock, StoredUndoableBlock undoableBlock) throws BlockStoreException {
        super.put(storedBlock, undoableBlock);
        // The blockHash to height link is now known, so index the commitment by its block height
        tryCommitPendingMerkleRootCommitment(storedBlock.getHeight(), storedBlock.getHeader().getHash());
    }

    @Override
    public void setVerifiedChainHead(StoredBlock chainHead) throws BlockStoreException {
        // Ignore when opening database and setting the genesis block as chainhead before the constructor is finished
        if (chainHead.getHeight() == 0) {
            super.setVerifiedChainHead(chainHead);
            return;
        }

        try {
            // Clean too old versioned data
            if (versioningMaxAge > 0) {
                int heightLimit = chainHead.getHeight() - versioningMaxAge;
                removeOldVersionedUTXOShards(heightLimit);
                removeOldVersionedMerkleNodes(heightLimit);
                removeOldMerkleRootCommitments(heightLimit);
            }
            // Still need to store something, even if it's an empty list, to not raise an exception when reading it during initialization
            storeShardsHistory();
            storeVersionedMerkleNodes();

            // Flush new versioned shards into the database, delete the original shards, and update the cache
            for (UTXOShardKey shardKey : batchVersionedUTXOShards.keySet()) {
                UTXOShard shard = batchVersionedUTXOShards.get(shardKey);
                putVersionedUTXOShard(shard, chainHead.getHeight(), true);
            }
            batchVersionedUTXOShards = null;
            batchVersionedUTXOs = null;

            // Flush new shards into database and update the main cache
            for (DatabaseKey dbKey : batchUTXOShards.keySet()) {
                UTXOShard shard = batchUTXOShards.get(dbKey);
                shard.updateCreationDate(chainHead.getHeader().getHash(), chainHead.getHeight());
                putUTXOShard(shard, true);
            }
            batchUTXOShards = null;

            super.setVerifiedChainHead(chainHead);
        } catch (DBException e) {
            throw new BlockStoreException(e);
        }
    }

    /**
     * UTXO shards methods
     */

    public DatabaseKey getUTXOShardDbKey(UTXOShardKey shardKey) {
        byte[] dbKey = new byte[DB_KEY_LENGTH_ONE_HASH];
        dbKey[0] = (byte) KeyTypeExtended.UTXO_SHARDS_CURRENT.ordinal();
        System.arraycopy(shardKey.getBytes(), 0, dbKey, 1, Sha256Hash.LENGTH);
        return DatabaseKey.wrap(dbKey);
    }

    public DatabaseKey getVersionedUTXOShardDbKey(UTXOShardKey shardKey, int deletionHeight) {
        byte[] dbKey = new byte[DB_KEY_LENGTH_ONE_INT_ONE_HASH];
        dbKey[0] = (byte) KeyTypeExtended.UTXO_SHARDS_VERSIONED.ordinal();
        Utils.uint32ToByteArrayLE(deletionHeight, dbKey, 1);
        System.arraycopy(shardKey.getBytes(), 0, dbKey, 1 + 4, Sha256Hash.LENGTH);
        return DatabaseKey.wrap(dbKey);
    }

    public DatabaseKey getVersionedUTXODbKey(Sha256Hash hash, long index) {
        byte[] dbKey = new byte[DB_KEY_LENGTH_ONE_HASH_ONE_LONG];
        dbKey[0] = (byte) KeyTypeExtended.UTXO_VERSIONED.ordinal();
        System.arraycopy(hash.getBytes(), 0, dbKey, 1, Sha256Hash.LENGTH);
        Utils.uint64ToByteArrayLE(index, dbKey, 1 + Sha256Hash.LENGTH);
        return DatabaseKey.wrap(dbKey);
    }

    public UTXOShard getUTXOShard(UTXOShardKey shardKey) throws BlockStoreException {
        beginMethod("getUTXOShard");
        try {
            UTXOShard result = null;
            DatabaseKey dbKey = getUTXOShardDbKey(shardKey);

            if (autoCommit) {
                result = utxoShardCache.get(dbKey);
            } else {
                // Check if we have an uncommitted entry
                result = batchUTXOShards.get(dbKey);
                if (result == null) {
                    // And lastly above check if we have a committed cached entry
                    result = utxoShardCache.get(dbKey);
                }
            }
            if (result != null) {
                hit++;
                return result;
            }
            miss++;
            // If we get here have to hit the database.
            byte[] inbytes = batchGet(dbKey.getBytes());
            if (inbytes == null) {
                return null;
            }
            ByteArrayInputStream bis = new ByteArrayInputStream(inbytes);
            UTXOShard shard = new UTXOShard(getParams(), bis);
            return shard;
        } catch (IOException e) {
            throw new BlockStoreException("Error getting UTXO shard key: " + shardKey.getBytes());
        } finally {
            endMethod("getUTXOShard");
        }
    }

    public void putUTXOShard(UTXOShard shard, boolean writeThrough) throws BlockStoreException {
        beginMethod("putUTXOShard");
        try {
            if (shard.getOutPoints().size() == 0) {
                // Only add the shard if there is something in it
                log.warn("Trying to add a UTXO shard with no outpoints " + shard);
                return;
            }

            DatabaseKey dbKey = getUTXOShardDbKey(shard.getKey());
            if (autoCommit || writeThrough) {
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                try {
                    shard.serializeToStream(bos);
                } catch (IOException e) {
                    throw new BlockStoreException("Error serializing utxo shard", e);
                }
                utxoShardCache.put(dbKey, shard);
                batchPut(dbKey.getBytes(), bos.toByteArray());
            } else {
                batchUTXOShards.put(dbKey, shard);
            }
        } finally {
            endMethod("putUTXOShard");
        }
    }

    public void removeUTXOShard(UTXOShardKey shardKey, boolean writeThrough) throws BlockStoreException {
        beginMethod("removeUTXOShard");
        try {
            DatabaseKey dbKey = getUTXOShardDbKey(shardKey);
            batchUTXOShards.remove(dbKey);

            if (autoCommit || writeThrough) {
                utxoShardCache.remove(dbKey);
                batchDelete(dbKey.getBytes());
            }
        } finally {
            endMethod("removeUTXOShard");
        }
    }

    public UTXOShard getVersionedUTXOShard(UTXOShardKey shardKey, int deletionHeight) throws BlockStoreException {
        if (versioningMaxAge < 1) return null;
        beginMethod("getVersionedUTXOShard");
        try {

            UTXOShard result = null;
            DatabaseKey dbKey = getVersionedUTXOShardDbKey(shardKey, deletionHeight);

            if (!autoCommit) {
                // Check if we have an uncommitted entry
                result = batchVersionedUTXOShards.get(shardKey);
                if (result != null) {
                    return result;
                }
            }

            byte[] inbytes = batchGet(dbKey.getBytes());
            if (inbytes == null) {
                return null;
            }
            ByteArrayInputStream bis = new ByteArrayInputStream(inbytes);
            UTXOShard shard = new UTXOShard(getParams(), bis);
            return shard;
        } catch (IOException e) {
            throw new BlockStoreException("Error getting versioned shard with shardKey: " +
                    shardKey.getBytes() + ", and height: " + deletionHeight, e);
        } finally {
            endMethod("getVersionedUTXOShard");
        }
    }

    public UTXOShard getVersionedUTXOShard(VersionedUTXOShardIndex vShard) throws BlockStoreException {
        return getVersionedUTXOShard(vShard.getShardKey(), vShard.getDeletionHeight());
    }

    public void putVersionedUTXOShard(UTXOShard shard, int deletionHeight, boolean writeThrough) throws BlockStoreException {
        if (versioningMaxAge < 1) return;
        if (shard.getOutPoints().size() == 0) {
            log.warn("Trying to version a UTXO shard with no outpoints " + shard.getKey());
        }
        beginMethod("putVersionedUTXOShard");
        try {
            VersionedUTXOShardIndex vShardIndex = new VersionedUTXOShardIndex(deletionHeight, shard.getKey());
            if (autoCommit || writeThrough) {
                DatabaseKey dbKey = getVersionedUTXOShardDbKey(shard.getKey(), deletionHeight);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                try {
                    shard.serializeToStream(bos);
                } catch (IOException e) {
                    throw new BlockStoreException("Error serializing utxo", e);
                }
                batchPut(dbKey.getBytes(), bos.toByteArray());

                // Make it easily findable when cleaning up old shards
                versionedShards.add(vShardIndex);

                // Store the UTXOs alongside it
                for (UTXO utxo : batchVersionedUTXOs.get(vShardIndex)) {
                    putVersionedUTXO(utxo.getHash(), utxo.getIndex(), utxo);
                }
            } else {
                if (!batchVersionedUTXOShards.containsKey(shard.getKey())) {
                    batchVersionedUTXOShards.put(shard.getKey(), shard);

                    // Store the UTXOs in a batch as well
                    List<UTXO> utxos = getUTXOsInShard(shard);
                    batchVersionedUTXOs.put(vShardIndex, utxos);
                }
            }
        } finally {
            endMethod("putVersionedUTXOShard");
        }
    }

    public void removeVersionedUTXOShard(VersionedUTXOShardIndex vShard) throws BlockStoreException {
        if (versioningMaxAge < 1) return;
        beginMethod("removeVersionedUTXOShard");
        try {
            for (TransactionOutPoint outpoint : getVersionedUTXOShard(vShard).getOutPoints()) {
                removeVersionedUTXO(outpoint.getHash(), outpoint.getIndex());
            }
            DatabaseKey dbKey = getVersionedUTXOShardDbKey(vShard.getShardKey(), vShard.getDeletionHeight());
            batchDelete(dbKey.getBytes());
        } finally {
            endMethod("removeVersionedUTXOShard");
        }
    }

    public UTXO getVersionedUTXO(Sha256Hash hash, long index) throws BlockStoreException {
        if (versioningMaxAge < 1) return null;
        beginMethod("getVersionedUTXO");
        try {
            DatabaseKey dbKey = getVersionedUTXODbKey(hash, index);
            byte[] inbytes = batchGet(dbKey.getBytes());
            if (inbytes == null) {
                return null;
            }
            ByteArrayInputStream bis = new ByteArrayInputStream(inbytes);
            return new UTXO(bis);
        } catch (IOException e) {
            throw new BlockStoreException("Error getting versioned UTXO: " + hash + ":" + index);
        } finally {
            endMethod("getVersionedUTXO");
        }
    }

    public void putVersionedUTXO(Sha256Hash hash, long index, UTXO utxo) throws BlockStoreException {
        if (versioningMaxAge < 1) return;
        beginMethod("putVersionedUTXO");
        try {
            DatabaseKey dbKey = getVersionedUTXODbKey(hash, index);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            try {
                utxo.serializeToStream(bos);
            } catch (IOException e) {
                throw new BlockStoreException("Error serializing utxo", e);
            }
            batchPut(dbKey.getBytes(), bos.toByteArray());
        } finally {
            endMethod("putVersionedUTXO");
        }
    }

    public void removeVersionedUTXO(Sha256Hash hash, long index) throws BlockStoreException {
        if (versioningMaxAge < 1) return;
        beginMethod("removeVersionedUTXO");
        try {
            DatabaseKey dbKey = getVersionedUTXODbKey(hash, index);
            batchDelete(dbKey.getBytes());
        } finally {
            endMethod("removeVersionedUTXO");
        }
    }

    /** Return the list of UTXOs which outpoints are in the shard
     * @throws BlockStoreException */
    public List<UTXO> getUTXOsInShard(UTXOShard shard) throws BlockStoreException {
        List<UTXO> utxos = new LinkedList<UTXO>();
        for (TransactionOutPoint outpoint : shard.getOutPoints()) {
            UTXO utxo = getTransactionOutput(outpoint.getHash(), outpoint.getIndex());
            if (utxo == null) {
                log.error("UTXO shard stores a link to an unknown UTXO: " + outpoint.getHash() + ":" + outpoint.getIndex());
                continue;
            }
            utxos.add(utxo);
        }
        return utxos;
    }

    /** Return the list of UTXOs which outpoints are in the versioned shard
     * @throws BlockStoreException */
    public List<UTXO> getUTXOsInVersionedShard(UTXOShard shard) throws BlockStoreException {
        List<UTXO> utxos = new LinkedList<UTXO>();
        for (TransactionOutPoint outpoint : shard.getOutPoints()) {
            UTXO utxo = getVersionedUTXO(outpoint.getHash(), outpoint.getIndex());
            if (utxo == null) {
                log.error("UTXO shard stores a link to an unknown versioned UTXO: " + outpoint.getHash() + ":" + outpoint.getIndex());
                continue;
            }
            utxos.add(utxo);
        }
        return utxos;
    }

    /**
     * Return all the shards needed to verify and affected by the asked block hash, wrapper for getMostFittingShard().
     * @param blockHash hash of the block to get verified
     * @param withUTXOs wether the UTXOs should be provided
     * @return a set containing all the shards affected by block blockHash
     * @throws BlockStoreException
     */
    public List<UTXOShardWithUTXOs> getShardsModifiedByBlock(Sha256Hash blockHash, boolean withUTXOs) throws BlockStoreException {
        beginMethod("getShardsModifiedByBlock");
        try {
            StoredBlock sBlock = get(blockHash);
            if (sBlock == null) {
                // Unknown block
                return null;
            }
            // Need to use the previous block hash to verify this block
            Sha256Hash prevBlockHash = sBlock.getHeader().getPrevBlockHash();

            List<UTXOShardWithUTXOs> shards = new LinkedList<UTXOShardWithUTXOs>();
            for (UTXOShardKey key : PrefixShardingPolicy.getAllKeysFor(blockHash, this)) {
                UTXOShardWithUTXOs shard = getMostFittingShard(key, prevBlockHash, withUTXOs);
                if (shard != null) {
                    shards.add(shard);
                }
            }
            return shards;
        } finally {
            endMethod("getShardsModifiedByBlock");
        }
    }

    /**
     * Find the shard with the given pair (shard key, creationDate) such that creationDate <= queryDate
     * and no other shard with this key has a higher creationDate.
     * @param shardKey identifier of the shard
     * @param queryDate most recent possible creationDate for the shard
     * @param withUTXOs wether the UTXOs should be provided
     * @return desired shard with its accompanying UTXOs, or null if not found
     * @throws BlockStoreException
     */
    public UTXOShardWithUTXOs getMostFittingShard(UTXOShardKey shardKey, Sha256Hash queryDate, boolean withUTXOs) throws BlockStoreException {
        beginMethod("getMostFittingShard");
        try {
            if (versioningMaxAge < 1) {
                // We don't store versioned data and it's best to make the weakest assumption on
                // what is stored in cache if their is no versioned data
                return null;
            }
            if (!PrefixShardingPolicy.checkKeyValidity(shardKey)) {
                return null;
            }

            StoredBlock b = get(queryDate);
            if (b == null) {
                // Asking for a node from a block we haven't received yet
                return null;
            }
            int queryHeight = get(queryDate).getHeight();

            // Most recent shard?
            UTXOShard shard = getUTXOShard(shardKey);
            if (queryHeight >= shard.getCreationBlockHeight()) {
                if (withUTXOs) {
                    return new UTXOShardWithUTXOs(shard, getUTXOsInShard(shard));
                } else {
                    return new UTXOShardWithUTXOs(shard, null);
                }
            }

            // See if there is a matching versioned shards
            for (int deletionHeight = queryHeight + 1; deletionHeight <= getVerifiedChainHead().getHeight(); deletionHeight++) {
                shard = getVersionedUTXOShard(shardKey, deletionHeight);
                if (shard != null) {
                    if (queryHeight < shard.getCreationBlockHeight()) {
                        // The query concerns a shard that is older than the oldest versioned shard stored
                        return null;
                    }
                    if (withUTXOs) {
                        return new UTXOShardWithUTXOs(shard, getUTXOsInVersionedShard(shard));
                    } else {
                        return new UTXOShardWithUTXOs(shard, null);
                    }
                }
            }

            // Query may have an incorrect shard key or may be asking for a too old shard
            return null;
        } finally {
            endMethod("getMostFittingShard");
        }
    }

    /** Remove shards that passed the age limit and update the stored list of versioned shards */
    protected void removeOldVersionedUTXOShards(int heightLimit) throws BlockStoreException {
        while (!versionedShards.isEmpty()) {
            VersionedUTXOShardIndex vShard = versionedShards.getFirst();
            if (vShard.getDeletionHeight() > heightLimit) {
                break;
            }
            removeVersionedUTXOShard(vShard);
            versionedShards.remove(vShard);
        }
    }

    /**
     * UTXO Merkle tree methods
     */

    /** Return the index of a shard in the UTXO Merkle tree **/
    public int getMerkleLeafIndex(UTXOShardKey shardKey) {
        // Breadth-first complete tree traversal for serialization
        // index = 0
        // for each bit in flags:
        //     index = 2 * index + 1 + bit
        byte[] array = shardKey.getBytes();
        int index = 0;
        for (int i = PrefixShardingPolicy.getKeyLengthInBit()-1; i >= 0; i--) {
            index = 2 * index + 1 + (Utils.checkBitLE(array, i) ? 1 : 0);
        }
        return index;
    }

    /** Return the dbKey needed to access the hash of a UTXO Merkle node from its index in the tree **/
    public DatabaseKey getMerkleNodeDbKey(int index) {
        byte[] dbKey = new byte[DB_KEY_LENGTH_ONE_INT];
        dbKey[0] = (byte) KeyTypeExtended.UTXO_MERKLE_TREE_CURRENT.ordinal();
        Utils.uint32ToByteArrayLE(index, dbKey, 1);
        return DatabaseKey.wrap(dbKey);
    }

    /** Return the hash of a node in the UTXO Merkle tree **/
    public Sha256Hash getMerkleNode(int index) throws BlockStoreException {
        beginMethod("getMerkleNode");
        try {
            Sha256Hash result = null;

            if (index < merkleNodesCacheSize) {
                if (autoCommit) {
                    result = merkleNodeCache.get(index);
                } else {
                    // Check if we have an uncommitted delete.
                    if (merkleNodeUncommittedDeletedCache.contains(index)) {
                        hit++;
                        return result;
                    }
                    // Check if we have an uncommitted entry
                    result = merkleNodeUncommittedCache.get(index);
                    if (result == null) {
                        // And lastly above check if we have a committed cached entry
                        result = merkleNodeCache.get(index);
                    }
                }
                if (result != null) {
                    hit++;
                    return result;
                }
                miss++;
            }

            // Not found in cache
            DatabaseKey dbKey = getMerkleNodeDbKey(index);
            byte[] inbytes = batchGet(dbKey.getBytes());
            if (inbytes == null) {
                return null;
            }
            ByteArrayInputStream bis = new ByteArrayInputStream(inbytes);
            return Sha256Hash.fromStream(bis);
        } catch (IOException e) {
            throw new BlockStoreException("IOException while reading a merkle node from database");
        } finally {
            endMethod("getMerkleNode");
        }
    }

    public void putMerkleNode(int index, Sha256Hash value) throws BlockStoreException {
        beginMethod("putMerkleNode");
        try {
            DatabaseKey dbKey = getMerkleNodeDbKey(index);
            batchPut(dbKey.getBytes(), value.getBytes());
            if (index < merkleNodesCacheSize) {
                if (autoCommit) {
                    merkleNodeCache.put(index, value);
                } else {
                    merkleNodeUncommittedCache.put(index, value);
                    merkleNodeUncommittedDeletedCache.remove(index);
                }
            }
        } finally {
            endMethod("putMerkleNode");
        }
    }

    public void removeMerkleNode(int index) throws BlockStoreException {
        beginMethod("removeMerkleNode");
        try {
            if (index < merkleNodesCacheSize) {
                if (autoCommit) {
                    merkleNodeCache.remove(index);
                } else {
                    merkleNodeUncommittedDeletedCache.add(index);
                    merkleNodeUncommittedCache.remove(index);
                }
            }
            DatabaseKey dbKey = getMerkleNodeDbKey(index);
            batchDelete(dbKey.getBytes());
        } finally {
            endMethod("removeMerkleNode");
        }
    }

    /**
     * Find the Merkle node with the given pair (index, creationDate) such that creationDate <= queryDate
     * and no other node with this index has a higher creationDate.
     * @param index index of the node in the tree
     * @param queryDate most recent possible creationDate for the node
     * @return desired node's hash or null if not found
     * @throws BlockStoreException
     */
    public Sha256Hash getMostFittingMerkleNode(int index, Sha256Hash queryDate) throws BlockStoreException {
        beginMethod("getMostFittingMerkleNode");
        try {
            if (versioningMaxAge < 1) {
                // We don't store versioned data and it's best to make the weakest assumption on
                // what is stored in cache if there is no versioned data
                return null;
            }

            // Ensure index validity
            if (index < 0 || index >= (int) Math.pow(2, PrefixShardingPolicy.getKeyLengthInBit() + 1) - 1) {
                log.warn("Received query for a most fitting node with an out of bound index of " + index);
                return null;
            }

            StoredBlock b = get(queryDate);
            if (b == null) {
                // Asking for a node from a block we haven't received yet
                return null;
            }
            int queryHeight = get(queryDate).getHeight();

            if (queryHeight < getChainHeadHeight() - versioningMaxAge) {
                // Asking for a too old node, merkle node creation date are not stored
                // so it's better to be conservative here
                return null;
            }

            // No need to parse the old versions if there is no current one
            Sha256Hash recentValue = getMerkleNode(index);
            if (recentValue == null) {
                return null;
            }

            // See if there is a matching versioned node
            for (int deletionHeight = queryHeight + 1; deletionHeight <= getVerifiedChainHead().getHeight(); deletionHeight++) {
                Sha256Hash node = versionedMerkleNodes.get(new VersionedMerkleTreeNodeIndex(deletionHeight, index));
                if (node != null) {
                    return node;
                }
            }

            // Must be the most recent node
            return recentValue;
        } catch (UTXOProviderException e) {
            throw new BlockStoreException(e);
        } finally {
            endMethod("getMostFittingMerkleNode");
        }
    }

    public Sha256Hash getVersionedMerkleNode(VersionedMerkleTreeNodeIndex index) {
        if (versioningMaxAge < 1) return null;
        return versionedMerkleNodes.get(index);
    }

    public Sha256Hash putVersionedMerkleNode(VersionedMerkleTreeNodeIndex index, Sha256Hash hash) {
        if (versioningMaxAge < 1) return null;
        return versionedMerkleNodes.put(index, hash);
    }

    /** Remove merkle nodes that passed the age limit and update the stored list of versioned Merkle nodes */
    protected void removeOldVersionedMerkleNodes(int heightLimit) throws BlockStoreException {
        Set<VersionedMerkleTreeNodeIndex> nodesToDelete = new HashSet<VersionedMerkleTreeNodeIndex>();
        for (Iterator<VersionedMerkleTreeNodeIndex> it = versionedMerkleNodes.keySet().iterator(); it.hasNext();) {
            VersionedMerkleTreeNodeIndex node = it.next();
            if (node.getDeletionHeight() > heightLimit) {
                break;
            }
            nodesToDelete.add(node);
        }
        for (VersionedMerkleTreeNodeIndex node : nodesToDelete) {
            versionedMerkleNodes.remove(node);
        }
    }

    public void updateMerkleTree(StoredBlock chainHead) throws BlockStoreException {
        UTXOPartialMerkleTreeMessage.updateTreeDatabase(this, chainHead.getHeight(), chainHead.getHeader().getHash());
    }

    /**
     * UTXO Merkle root commitments
     */

    public DatabaseKey getMerkleRootCommitmentDbKey(int blockHeight) {
        byte[] dbKey = new byte[DB_KEY_LENGTH_ONE_INT];
        dbKey[0] = (byte) KeyTypeExtended.UTXO_MERKLE_ROOT_COMMITMENTS.ordinal();
        Utils.uint32ToByteArrayLE(blockHeight, dbKey, 1);
        return DatabaseKey.wrap(dbKey);
    }

    public DatabaseKey getPendingMerkleRootCommitmentDbKey(Sha256Hash blockHash) {
        byte[] dbKey = new byte[DB_KEY_LENGTH_ONE_HASH];
        dbKey[0] = (byte) KeyTypeExtended.UTXO_MERKLE_ROOT_COMMITMENTS.ordinal();
        System.arraycopy(blockHash.getBytes(), 0, dbKey, 1, Sha256Hash.LENGTH);
        return DatabaseKey.wrap(dbKey);
    }

    public Sha256Hash getMerkleRootCommitment(int blockHeight) throws BlockStoreException {
        beginMethod("getMerkleRootCommitment");
        try {
            DatabaseKey dbKey = getMerkleRootCommitmentDbKey(blockHeight);
            byte[] inbytes = batchGet(dbKey.getBytes());
            if (inbytes == null) {
                return null;
            }
            ByteArrayInputStream bis = new ByteArrayInputStream(inbytes);
            return Sha256Hash.fromStream(bis);
        } catch (IOException e) {
            throw new BlockStoreException("Error getting Merkle root commitment from block " + blockHeight, e);
        } finally {
            endMethod("getMerkleRootCommitment");
        }
    }

    public Sha256Hash getMerkleRootPendingCommitment(Sha256Hash blockHash) throws BlockStoreException {
        beginMethod("getMerkleRootPendingCommitment");
        try {
            DatabaseKey dbKey = getPendingMerkleRootCommitmentDbKey(blockHash);
            byte[] inbytes = batchGet(dbKey.getBytes());
            if (inbytes == null) {
                return null;
            }
            ByteArrayInputStream bis = new ByteArrayInputStream(inbytes);
            return Sha256Hash.fromStream(bis);
        } catch (IOException e) {
            throw new BlockStoreException("Error getting pending Merkle root commitment from block " + blockHash, e);
        } finally {
            endMethod("getMerkleRootPendingCommitment");
        }
    }

    protected void putMerkleRootCommitment(int blockHeight, Sha256Hash root) throws BlockStoreException {
        beginMethod("putMerkleRootCommitment");
        try {
            batchPut(getMerkleRootCommitmentDbKey(blockHeight).getBytes(), root.getBytes());
            // Keep the oldest merkle root to know which ones to remove
            if (heightOldestMerkleRootCommitment > blockHeight) {
                heightOldestMerkleRootCommitment = blockHeight;
                storeHeightOldestMerkleRootCommitment();
            }
        } finally {
            endMethod("putMerkleRootCommitment");
        }
    }

    public void putPendingMerkleRootCommitment(Sha256Hash blockHash, Sha256Hash root) {
        beginMethod("putPendingMerkleRootCommitment");
        try {
            batchPut(getPendingMerkleRootCommitmentDbKey(blockHash).getBytes(), root.getBytes());
        } finally {
            endMethod("putPendingMerkleRootCommitment");
        }
    }

    public void removeMerkleRootCommitment(int blockHeight) {
        beginMethod("removeMerkleRootCommitment");
        try {
            batchDelete(getMerkleRootCommitmentDbKey(blockHeight).getBytes());
        } finally {
            endMethod("removeMerkleRootCommitment");
        }
    }

    public void removePendingMerkleRootCommitment(Sha256Hash blockHash) {
        beginMethod("removePendingMerkleRootCommitment");
        try {
            batchDelete(getPendingMerkleRootCommitmentDbKey(blockHash).getBytes());
        } finally {
            endMethod("removePendingMerkleRootCommitment");
        }
    }

    public int readHeightOldestMerkleRootCommitment() throws BlockStoreException {
        beginMethod("readHeightOldestMerkleRootCommitment");
        try {
            byte[] bytes = batchGet(getKey(KeyTypeExtended.HEIGHT_OLDEST_MERKLE_ROOT_COMMITMENT));
            ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
            return (int) Utils.readUint32FromByteStreamLE(bis);
        } catch (IOException e) {
            throw new BlockStoreException("Error loading the height of the oldest Merkle root commitment: " + e);
        } finally {
            endMethod("readHeightOldestMerkleRootCommitment");
        }
    }

    public void storeHeightOldestMerkleRootCommitment() throws BlockStoreException {
        beginMethod("storeHeightOldestMerkleRootCommitment");
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            Utils.uint32ToByteStreamLE(heightOldestMerkleRootCommitment, bos);
            batchPut(getKey(KeyTypeExtended.HEIGHT_OLDEST_MERKLE_ROOT_COMMITMENT), bos.toByteArray());
        } catch (IOException e) {
            throw new BlockStoreException("Error storing the height of the oldest Merkle root commitment: " + e);
        } finally {
            endMethod("storeHeightOldestMerkleRootCommitment");
        }
    }

    /** Remove merkle root commitments that passed the age limit and update the stored list */
    protected void removeOldMerkleRootCommitments(int heightLimit) throws BlockStoreException {
        if (heightOldestMerkleRootCommitment == Integer.MAX_VALUE) {
            // The value has never been changed => no commitments stored
            return;
        }
        while (heightOldestMerkleRootCommitment < heightLimit) {
            removeMerkleRootCommitment(heightOldestMerkleRootCommitment);
            heightOldestMerkleRootCommitment++;
        }
        storeHeightOldestMerkleRootCommitment();
    }

    /** Commitments are extracted from blocks asap and can only be committed once we know the block height */
    protected void tryCommitPendingMerkleRootCommitment(int blockHeight, Sha256Hash blockHash) throws BlockStoreException {
        if (blockHeight == 0) {
            return;
        }
        Sha256Hash root = getMerkleRootPendingCommitment(blockHash);
        if (root != null) {
            putMerkleRootCommitment(blockHeight, root);
            removePendingMerkleRootCommitment(blockHash);
        }
    }

    /**
     * Debug method to display stats on runtime of each method and cache hit rates etc..
     * */
    @Override
    public void dumpStats() {
        long wallTimeNanos = totalStopwatch.elapsed(TimeUnit.NANOSECONDS);
        long dbtime = 0;
        for (String name : methodCalls.keySet()) {
            long calls = methodCalls.get(name);
            long time = methodTotalTime.get(name);
            dbtime += time;
            long average = time / calls;
            double proportion = (time + 0.0) / (wallTimeNanos + 0.0);
            log.info(name + " c:" + calls + " r:" + time + " a:" + average + " p:" + String.format("%.2f", proportion));
        }
        double dbproportion = (dbtime + 0.0) / (wallTimeNanos + 0.0);
        double hitrate = (hit + 0.0) / (hit + miss + 0.0);
        log.info("Cache size:" + utxoShardCache.size() + " hit:" + hit + " miss:" + miss + " rate:"
                + String.format("%.2f", hitrate));
        bloom.printStat();
        log.info("hasTxOut call:" + hasCall + " True:" + hasTrue + " False:" + hasFalse);
        log.info("Wall:" + totalStopwatch + " percent:" + String.format("%.2f", dbproportion));
        String stats = db.getProperty("leveldb.stats");
        System.out.println(stats);
    }

    /**
     * Debug method to display various stats on the Dietcoin added data
     */

    /**
     * Debug method to display the total UTXO set size and its hash when serialized.
     * Used to compare the results between a LevelDBShardedBlockStore and a LevelDBFullPrunedBlockStore.
     */
    @Override
    public void dumpUTXOSetStats(){
        List<byte[]> allUtxos = new ArrayList<byte[]>();

        // Scan the database to add all the UTXOs to the array
        // Probably could be faster by using an iterator like in super.dumpUTXOSetStats()
        try {
            List<UTXOShardKey> shardKeys = PrefixShardingPolicy.generateAllKeys();
            for (UTXOShardKey shardKey : shardKeys) {
                UTXOShard shard = getUTXOShard(shardKey);
                if (shard == null) continue;
                for (TransactionOutPoint outpoint : shard.getOutPoints()) {
                    UTXO utxo = getTransactionOutput(outpoint.getHash(), outpoint.getIndex());
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    utxo.serializeToStream(bos);
                    allUtxos.add(bos.toByteArray());
                }
            }
        } catch (BlockStoreException e) {
            log.error("Error getting a shard while dumping UTXO set stats: ", e);
            return;
        } catch (IOException e) {
            log.error("Error serializing a shard while dumping UTXO set stats: ", e);
            return;
        }

        // Sort all the serialized utxos
        Collections.sort(allUtxos, new ByteArrayComparator());

        // Find the length of the serialized UTXO set
        int utxoSetLength = 0;
        for (byte[] utxo : allUtxos) {
            utxoSetLength += utxo.length;
        }

        // Serialize the entire UTXO set into one byte buffer
        System.out.println("WARNING: about to serialize the UTXO set, this can take a lot of RAM, or swap... :)");
        byte[] utxoSet = new byte[utxoSetLength];
        int position = 0;
        for (byte[] utxo : allUtxos) {
            System.arraycopy(utxo, 0, utxoSet, position, utxo.length);
            position += utxo.length;
        }
        Sha256Hash utxoSetHash = Sha256Hash.twiceOf(utxoSet);
        System.out.println("UTXO set hash: " + utxoSetHash + "\titems: "
                + allUtxos.size() + "\tsize: " + String.format("%.3f", utxoSetLength / 1024.0) + " KiB");
    }

    /**
     * Debug method to display the size and the hash of each UTXO shard, one line per shard.
     */
    public void dumpUTXOShardsStats(){
        try {
            int i = -1;
            List<UTXOShardKey> shardKeys = PrefixShardingPolicy.generateAllKeys();
            for (UTXOShardKey shardKey : shardKeys) {
                i++;
                UTXOShard shard = getUTXOShard(shardKey);
                if (shard == null) continue;
                System.out.println(i + "\t" + shard.toString());
            }
        } catch (BlockStoreException e) {
            log.error("Error getting a shard while dumping UTXO shard stats: ", e);
        }
    }

    /**
     * Debug method to display the size, hash and deletion date of each versioned UTXO shard, one line per shard.
     */
    public void dumpVersionedUTXOShardsStats() {
        try {
            int i = -1;
            for (VersionedUTXOShardIndex versionedShard : versionedShards) {
                i++;
                System.out.println(i + "\t" + getVersionedUTXOShard(versionedShard)
                + "\tdeletion height: " + versionedShard.getDeletionHeight());
            }
        } catch (BlockStoreException e) {
            log.error("Error getting a shard while dumping UTXO shard stats: ", e);
        }
    }

    /**
     * Debug method to display the root, number of leaves and number of nodes in the UTXO Merkle tree.
     */
    public void dumpMerkleTreeStats() {
        try {
            System.out.println("WARNING: parsing the UTXO Merkle tree...");
            StringBuilder sb = new StringBuilder();
            int nbNodes = 0;
            int nbLeaves = 0;
            int maxIndex = (int) (2 * Math.pow(2, PrefixShardingPolicy.getKeyLengthInBit()) - 1);
            BigInteger bigModulo = BigInteger.valueOf(100000); // takes less space than a sha256
            for (int index = 0; index < maxIndex; index++) {
                Sha256Hash hash = getMerkleNode(index);
                if (!hash.equals(Sha256Hash.ZERO_HASH)) {
                    nbNodes++;
                    if (index > Math.pow(2, PrefixShardingPolicy.getKeyLengthInBit()) - 2) {
                        nbLeaves++;
                        sb.append("" + hash.toBigInteger().mod(bigModulo) + "  ");
                    }
                }
            }

            System.out.println("UTXO Merkle root hash: " + getMerkleNode(0) +
                    "\tkeyLength: " + PrefixShardingPolicy.getKeyLengthInBit() +
                    "\tleaves: " + nbLeaves + "\tnodes: " + nbNodes);
            System.out.println("UTXO Merkle leaves mod 10e5: " + sb.toString());
        } catch (BlockStoreException e) {
            log.error("Error getting a tree node while dumping UTXO Merkle tree stats: ", e);
        }
    }

    /**
     * Debug method to display the number of versioned Merkle tree nodes,
     * alongside the height of the first and last node.
     */
    public void dumpVersionedMerkleTreeStats() {
        System.out.println("WARNING: parsing the versioned UTXO Merkle tree...");
        StringBuilder sb = new StringBuilder();
        sb.append("Versioned UTXO Merkle nodes");
        sb.append("\tsize: " + versionedMerkleNodes.size());

        boolean firstItem = true;
        VersionedMerkleTreeNodeIndex lastItem = null;
        for (Iterator<VersionedMerkleTreeNodeIndex> it = versionedMerkleNodes.keySet().iterator(); it.hasNext();) {
            VersionedMerkleTreeNodeIndex item = it.next();
            if (firstItem) {
                firstItem = false;
                sb.append("\tfirst node height: " +  item.getDeletionHeight());
            }
            lastItem = item;
        }
        sb.append("\tlast node height: " + lastItem.getDeletionHeight());
        System.out.println(sb);
    }
}
