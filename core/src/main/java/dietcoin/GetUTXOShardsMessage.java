package dietcoin;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.bitcoinj.core.Message;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.core.ProtocolException;
import org.bitcoinj.core.Sha256Hash;
import org.bitcoinj.core.Utils;

public class GetUTXOShardsMessage extends Message {
    public static final String NAME = "getshards"; // "getutxoshards" is 13 char long but names are capped at 12

    private List<UTXOShard> shards;

    /** Used when sending a message, works in pair with bitcoinSerializeToStream(). */
    public GetUTXOShardsMessage(NetworkParameters params, Collection<UTXOShard> shards) {
        super(params);
        this.shards = new LinkedList<UTXOShard>(shards);
    }

    @Override
    protected void bitcoinSerializeToStream(OutputStream stream) throws IOException {
        Utils.uint32ToByteStreamLE(shards.size(), stream);
        for (UTXOShard shard : shards) {
            stream.write(Utils.reverseBytes(shard.getKey().getBytes()));
            stream.write(shard.getCreationBlockHash().getReversedBytes());
        }
    }

    /** Used when receiving a message, works in pair with parse(). */
    public GetUTXOShardsMessage(NetworkParameters params, byte[] payloadBytes) {
        super(params, payloadBytes, 0);
    }

    @Override
    protected void parse() throws ProtocolException {
        // Read the shards requested by a node.
        // - 4B nbShards
        //     - 32B key of shard #1
        //     - 32B date of shard #1
        //     ...
        shards = new LinkedList<UTXOShard>();
        long nbShards = readUint32();
        for (int i = 0; i < nbShards; i++) {
            UTXOShardKey key = UTXOShardKey.wrap(Utils.reverseBytes(readBytes(Sha256Hash.LENGTH)));
            Sha256Hash creationDate = readHash();
            shards.add(new UTXOShard(key, creationDate));
        }
        length = cursor;
    }

    public List<UTXOShard> getShards() {
        return shards;
    }
}
