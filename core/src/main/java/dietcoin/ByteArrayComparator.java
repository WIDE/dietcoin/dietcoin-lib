package dietcoin;

import java.util.Comparator;

// Comparator to sort maps and list of byte[]
public class ByteArrayComparator implements Comparator<byte[]> {
    @Override
    public int compare(byte[] a, byte[] b) {
        if (a.length < b.length) return -1;
        if (a.length > b.length) return 1;
        // Equal length arrays
        for (int i = 0; i < a.length; i++) {
            if (a[i] < b[i]) return -1;
            if (a[i] > b[i]) return 1;
        }
        // Equal length arrays with equal content
        return 0;
    }
}
