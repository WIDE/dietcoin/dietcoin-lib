package dietcoin;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import org.bitcoinj.core.UTXO;

// Java really needs Pair<>
// TODO DTC: make it extend UTXOShard and add the utxos?
public class UTXOShardWithUTXOs {
    private UTXOShard shard;
    private List<UTXO> utxos;

    public UTXOShardWithUTXOs(UTXOShard shard, List<UTXO> utxos) {
        this.shard = shard;
        this.utxos = utxos;
        if (utxos == null) {
            this.utxos = new LinkedList<UTXO>();
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        UTXOShardWithUTXOs cast = (UTXOShardWithUTXOs) obj;
        return shard.equals(cast.shard) && utxos.hashCode() == cast.utxos.hashCode();
    }

    @Override
    public int hashCode() {
        return Objects.hash(shard.hashCode(), utxos.hashCode());
    }

    public UTXOShard getShard() {
        return shard;
    }

    public List<UTXO> getUtxos() {
        return utxos;
    }
}
