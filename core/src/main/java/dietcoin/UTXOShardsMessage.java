package dietcoin;

import java.io.IOException;
import java.io.OutputStream;
import java.util.LinkedList;
import java.util.List;

import org.bitcoinj.core.Message;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.core.ProtocolException;
import org.bitcoinj.core.UTXO;
import org.bitcoinj.core.Utils;

public class UTXOShardsMessage extends Message {
    public static final String NAME = "shards";

    private List<UTXOShardWithUTXOs> shards;

    /** Used when sending a message, works in pair with bitcoinSerializeToStream(). */
    public UTXOShardsMessage(NetworkParameters params, List<UTXOShardWithUTXOs> shards) {
        super(params);
        this.shards = new LinkedList<UTXOShardWithUTXOs>(shards);
    }

    /** Used when sending a message */
    @Override
    protected void bitcoinSerializeToStream(OutputStream stream) throws IOException {
        Utils.uint32ToByteStreamLE(shards.size(), stream);
        for (UTXOShardWithUTXOs shardComplete : shards) {
            UTXOShard shard = shardComplete.getShard();
            List<UTXO> utxos = shardComplete.getUtxos();
            shard.bitcoinSerializeToStream(stream);
            Utils.uint32ToByteStreamLE(utxos.size(), stream);
            for (UTXO utxo : utxos) {
                utxo.bitcoinSerialize(stream);
            }
        }
    }

    /** Used when receiving a message, works in pair with parse(). */
    public UTXOShardsMessage(NetworkParameters params, byte[] payloadBytes) {
        super(params, payloadBytes, 0);
    }

    @Override
    protected void parse() throws ProtocolException {
        // Read multiple shards with their metadata and data
        // - 4B nbShards
        //     - xB metadata shard #1
        //     - 4B nbUTXOs in shard $1
        //         - xB UTXO #1
        //         ...
        //     ...
        long nbShards = readUint32();
        shards = new LinkedList<UTXOShardWithUTXOs>();
        for (int i = 0; i < nbShards; i++) {
            UTXOShard shard = new UTXOShard(params, payload, cursor);
            List<UTXO> utxos = new LinkedList<UTXO>();
            long nbUtxos = readUint32();
            for (int j = 0; j < nbUtxos; j++) {
                UTXO utxo = new UTXO(params, payload, cursor);
                utxos.add(utxo);
                shard.addOutPoint(utxo.getHash(), utxo.getIndex());
                cursor += utxo.getMessageSize();
            }
            shards.add(new UTXOShardWithUTXOs(shard, utxos));
            cursor += shard.getMessageSize();
        }
        length = cursor;
    }

    public List<UTXOShardWithUTXOs> getShards() {
        return shards;
    }
}
