package dietcoin;

import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.core.Sha256Hash;

public class UTXOMerkleRootMessage extends SingleHashMessage {
    public static final String NAME = "utxomroot";

    /** Used when sending a message, works in pair with bitcoinSerializeToStream(). */
    public UTXOMerkleRootMessage(NetworkParameters params, Sha256Hash blockHash) {
        super(params, blockHash);
    }

    /** Used when receiving a message, works in pair with parse(). */
    public UTXOMerkleRootMessage(NetworkParameters params, byte[] payloadBytes) {
        super(params, payloadBytes);
    }

    @Override
    public String getMessageName() {
        return NAME;
    }
}
