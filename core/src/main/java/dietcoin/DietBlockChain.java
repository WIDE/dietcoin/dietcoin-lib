package dietcoin;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

import javax.annotation.Nullable;

import org.bitcoinj.core.Block;
import org.bitcoinj.core.BlockChain;
import org.bitcoinj.core.Coin;
import org.bitcoinj.core.Context;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.core.Peer;
import org.bitcoinj.core.Sha256Hash;
import org.bitcoinj.core.StoredBlock;
import org.bitcoinj.core.StoredUndoableBlock;
import org.bitcoinj.core.Transaction;
import org.bitcoinj.core.TransactionInput;
import org.bitcoinj.core.TransactionOutPoint;
import org.bitcoinj.core.TransactionOutput;
import org.bitcoinj.core.UTXO;
import org.bitcoinj.core.VerificationException;
import org.bitcoinj.params.MainNetParams;
import org.bitcoinj.script.Script;
import org.bitcoinj.script.Script.VerifyFlag;
import org.bitcoinj.store.BlockStoreException;
import org.bitcoinj.utils.ContextPropagatingThreadFactory;
import org.bitcoinj.utils.VersionTally;
import org.bitcoinj.wallet.Wallet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A diet node is a mix of both an SPV node and a full node; it behaves like an SPV node for most
 * of the sync process and can behave like a full node if a block of interest is amongst the last
 * X blocks of the chain, with X parameterizable.
 * Due to its dual nature, the code of this class is inspired from both BlockChain (SPV node) and
 * FullPrunedBlockChain (full node).
 */
public class DietBlockChain extends BlockChain {
    private static final int DEFAULT_SUBCHAIN_LENGTH_VERIFICATION = 0; // Defaults to an SPV behavior

    private static final Logger log = LoggerFactory.getLogger(DietBlockChain.class);

    ExecutorService scriptVerificationExecutor = Executors.newFixedThreadPool(
            Runtime.getRuntime().availableProcessors(), new ContextPropagatingThreadFactory("Script verification"));

    protected final LevelDBShardedBlockStore blockStore;

    private int subchainLengthToVerify;

    // From AbstractBlockChain
    private final VersionTally versionTally;

    // 1- context, listeners, subchain length (not an inherited constructor)
    public DietBlockChain(Context context, List<Wallet> listeners, LevelDBShardedBlockStore blockStore, int subchainLengthToVerify) throws BlockStoreException {
        super(context, listeners, blockStore);
        this.blockStore = blockStore;
        this.subchainLengthToVerify = subchainLengthToVerify;
        this.versionTally = new VersionTally(context.getParams());
    }
    // 2- context, listeners: calls 1
    private DietBlockChain(Context context, List<Wallet> listeners, LevelDBShardedBlockStore blockStore) throws BlockStoreException {
        this(context, listeners, blockStore, DEFAULT_SUBCHAIN_LENGTH_VERIFICATION);
    }
    // 3- params, listeners: calls 2
    public DietBlockChain(NetworkParameters params, List<Wallet> listeners, LevelDBShardedBlockStore blockStore) throws BlockStoreException {
        this(Context.getOrCreate(params), listeners, blockStore);
    }

    // 4- context, wallet, subchain length (not an inherited constructor): calls 1
    public DietBlockChain(Context context, Wallet wallet, LevelDBShardedBlockStore blockStore, int subchainLengthToVerify) throws BlockStoreException {
        this(context, new ArrayList<Wallet>(), blockStore, subchainLengthToVerify);
        addWallet(wallet);
    }
    // 5- context, wallet: calls 4
    public DietBlockChain(Context context, Wallet wallet, LevelDBShardedBlockStore blockStore) throws BlockStoreException {
        this(context, wallet, blockStore, DEFAULT_SUBCHAIN_LENGTH_VERIFICATION);
    }
    // 6- params, wallet: calls 5
    public DietBlockChain(NetworkParameters params, Wallet wallet, LevelDBShardedBlockStore blockStore) throws BlockStoreException {
        this(Context.getOrCreate(params), wallet, blockStore);
    }

    // 7- context: calls 1
    public DietBlockChain(Context context, LevelDBShardedBlockStore blockStore, int subchainLengthToVerify) throws BlockStoreException {
        this(context, new ArrayList<Wallet>(), blockStore, subchainLengthToVerify);
    }
    // 8- context: calls 7
    public DietBlockChain(Context context, LevelDBShardedBlockStore blockStore) throws BlockStoreException {
        this(context, blockStore, DEFAULT_SUBCHAIN_LENGTH_VERIFICATION);
    }
    // 9- params: calls 8
    public DietBlockChain(NetworkParameters params, LevelDBShardedBlockStore blockStore) throws BlockStoreException {
        this(Context.getOrCreate(params), blockStore);
    }


    /** Add the block that may contain transactions to the store. Taken from FullPrunedBlockChain. */
    @Override
    protected StoredBlock addToBlockStore(StoredBlock storedPrev, Block block, boolean inMainChain)
            throws BlockStoreException, VerificationException {
        StoredBlock newBlock = storedPrev.build(block.cloneAsHeader());
        StoredUndoableBlock undoableBlock = blockStore.getUndoBlock(block.getHash());

        if (undoableBlock != null || block.getTransactionsMutable() == null) {
            // txs are already stored in a StoredUndoableBlock or the block is a header
            blockStore.put(newBlock);
        } else {
            // Store the height of the block in the transactions to later be able to rollback the UTXO set
            for (Transaction tx : block.getTransactionsMutable()) {
                tx.setInBlockOfHeight(newBlock.getHeight());
            }
            // txs aren't stored yet, need to store them
            blockStore.put(newBlock, new StoredUndoableBlock(newBlock.getHeader().getHash(), block.getTransactionsMutable()));
        }
        if (inMainChain) {
            storedPrev.setNextBlockHash(block.getHash());
            blockStore.put(storedPrev);
        }
        return newBlock;
    }

    @Override
    protected void rollbackBlockStore(int height) throws BlockStoreException {
        // FIXME DTC: handle rollbacks
        lock.lock();
        try {
            int currentHeight = getBestChainHeight();
            checkArgument(height >= 0 && height <= currentHeight, "Bad height: %s", height);
            if (height == currentHeight)
                return; // nothing to do

            // Look for the block we want to be the new chain head
            StoredBlock newChainHead = blockStore.getChainHead();
            while (newChainHead.getHeight() > height) {
                newChainHead = newChainHead.getPrev(blockStore);
                if (newChainHead == null)
                    throw new BlockStoreException("Unreachable height");
            }

            // Modify store directly
            blockStore.put(newChainHead);
            this.setChainHead(newChainHead);
        } finally {
            lock.unlock();
        }
    }

    @Override
    protected boolean shouldVerifyTransactions() {
        // Verify only some blocks, not all
        return false;
    }

    /**
     * The chainHead is the head of the headers, regardless of their block's transactions
     * verification status.
     */
    @Override
    protected void doSetChainHead(StoredBlock chainHead) throws BlockStoreException {
        blockStore.setChainHead(chainHead);
        blockStore.commitDatabaseBatchWrite();
    }

    @Override
    protected void notSettingChainHead() throws BlockStoreException {
        blockStore.abortDatabaseBatchWrite();
    }

    /**
     * The verifiedChainHead points to the most recent block which transactions have been verified
     * thanks to the UTXOs queried from other nodes.
     */
    protected void setVerifiedChainHead(StoredBlock verifiedChainHead) throws BlockStoreException {
        blockStore.setVerifiedChainHead(verifiedChainHead);
        blockStore.commitDatabaseBatchWrite();
    }




    /**
     * Called when the chain download is completed (at the same height as a peer), verify the correctness of
     * the latest subchain of blocks by downloading the UTXO shards modified by each block and ensuring their
     * authenticity by downloading a partial Merkle tree covering the UTXO shards.
     */
    public void onChainDownloadComplete(Peer peer) {
        /**
         * Diet node algorithm on chain download completion:
         * - compute which blocks to verify: either from height tip-subchainLength+1 to height of tip,
         *   or from tip-subchainLength+1 to a block of interest (e.g., containing a tx from this node's wallet)
         * - stored MRoot = query MRoot from the block previous to the first verified block, serves as a trust anchor
         * for each block to verify:
         * 1- query block
         *    sanity check block header and block transactions
         * 2- query partial MTree proving the shards modified by block
         * 3- build MTree, compute MRoot
         *    store MTree in database
         * 4- verify computed MRoot vs block stored MRoot
         *    (done before downloading shards because it's less bandwidth consuming)
         * 5- query shards for block
         * 6- verify shards against MTree leaves
         * 7- verify block thanks to the downloaded shards and update shards with block
         * 8- store updated MTree
         * 9- verify MRoot in block against the now updated MTree
         * 10- mark block as verified
         */

        // No block to verify => fallback to SPV
        if (subchainLengthToVerify < 1) {
            return;
        }

        // FIXME DTC: lock
//        lock.lock();
        try {
            // Find the list of blocks to verify by iterating from the chain head
            StoredBlock firstBlock = chainHead;
            StoredBlock lastBlock = chainHead;
            int firstBlockHeight = Math.max(1, chainHead.getHeight() - subchainLengthToVerify + 1); // don't verify the genesis block
            while (firstBlock.getHeight() > firstBlockHeight) {
                firstBlock = firstBlock.getPrev(blockStore);
            }

            // Download the trusted UTXO merkle root, from the block prior to the first verified blocks
            Sha256Hash trustedBlockHash = firstBlock.getPrev(blockStore).getHeader().getHash();
            Future<Sha256Hash> trustedUtxoMRoot = peer.getUTXOMerkleRootForBlock(trustedBlockHash);

            Sha256Hash storedMRoot = trustedUtxoMRoot.get();
            StoredBlock sBlock = firstBlock;
            while (sBlock != null && sBlock.getHeight() <= lastBlock.getHeight()) {
                Sha256Hash blockHash = sBlock.getHeader().getHash();
                int blockHeight = sBlock.getHeight();
                log.info("Starting verification of block " + blockHeight + " with hash " + blockHash);

                // Enable batched leveldb writing, commitment is made at doSetChainHead()
                blockStore.beginDatabaseBatchWrite();

                // 1- Get block
                Block block = peer.getBlock(blockHash).get();
                if (block == null) {
                    throw new VerificationException("Error downloading a block to verify: " + blockHash);
                }
                // The extraction of the UTXO Merkle root commitment from the block is done in Peer
                addToBlockStore(blockStore.get(blockHash).getPrev(blockStore), block);

                // Basic block header and transactions sanity check, no UTXO check here
                block.verify(blockHeight, params.getBlockVerificationFlags(block, versionTally, blockHeight));

                // 2- Download UTXO partial MTree complete enough to verify the block
                Future<UTXOPartialMerkleTreeMessage> futTree = peer.getUTXOPartialMerkleTree(blockHash);
                UTXOPartialMerkleTreeMessage tree = futTree.get();
                tree.setStore(blockStore); // Enable node storing while computing the root

                // 3- Compute UTXO MTree and its root, update the MTree in the database in the process
                List<Sha256Hash> matchedTreeLeaves = new ArrayList<Sha256Hash>();
                Sha256Hash computedMRoot = tree.getTxnHashAndMerkleRoot(matchedTreeLeaves);
                Set<Sha256Hash> shardsInTree = new HashSet<Sha256Hash>(matchedTreeLeaves);

                // 4- Verify downloaded MTree against block MRoot
                if (!computedMRoot.equals(storedMRoot)) {
                    throw new VerificationException("The computed UTXO Merkle root does not match"
                            + " the one in the block of height " + blockHeight + ", hash " + blockHash);
                }

                // 5- Download shards for block
                Future<List<UTXOShardWithUTXOs>> futShards = peer.getUTXOShardsForBlock(blockHash);
                List<UTXOShardWithUTXOs> shards = futShards.get();

                // 6- Verify shards against MTree
                if (shardsInTree.size() < shards.size()) {
                    throw new VerificationException("Not enough leaves in the UTXO MTree, can't check"
                            + " authenticity of all the shards");
                }
                for (UTXOShardWithUTXOs shardComplete : shards) {
                    if (!shardsInTree.contains(shardComplete.getShard().getHash())) {
                        throw new VerificationException("The UTXO shard with key " + shardComplete.getShard().getKey().toString()
                                + " is not in the Merkle tree sent by the remote peer, verification failed.");
                    }
                    blockStore.putUTXOShard(shardComplete.getShard(), false);
                    for (UTXO utxo : shardComplete.getUtxos()) {
                        blockStore.addUnspentTransactionOutput(utxo, blockHeight, blockHash);
                    }
                }

                // 7- Verify block transactions and update UTXO shards in the process
                verifyTransactions(block);

                // 8- Update UTXO MTree with the new shard hashes and extract the MRoot
                blockStore.updateMerkleTree(sBlock);
                storedMRoot = blockStore.getMerkleNode(0);

                // 9- Check stored MRoot against the one committed in block
                if (!storedMRoot.equals(blockStore.getMerkleRootCommitment(blockHeight))) {
                    throw new VerificationException("The updated UTXO Merkle root does not match"
                            + " the one in the block of height " + blockHeight + ", hash " + blockHash);
                }

                // 10- Mark block as verified
                setVerifiedChainHead(sBlock);

                // Prepare next iteration
                log.info("Complete verification of block " + blockHeight + " with hash " + blockHash);
                sBlock = blockStore.get(sBlock.getNextBlockHash());
            }
        } catch (BlockStoreException e) {
            // FIXME DTC: proper exceptions
            try { notSettingChainHead(); } catch (BlockStoreException e1) {}
            throw new RuntimeException(e);
        } catch (InterruptedException e) {
            try { notSettingChainHead(); } catch (BlockStoreException e1) {}
            throw new RuntimeException(e);
        } catch (ExecutionException e) {
            try { notSettingChainHead(); } catch (BlockStoreException e1) {}
            throw new RuntimeException(e);
        }
//        } finally {
//            lock.unlock();
//        }
    }

    /**
     * Code mostly taken from FullPrunedBlockchain.connectTransactions()
     */
    private void verifyTransactions(Block block) {
        // FIXME DTC: lock
//        checkState(lock.isHeldByCurrentThread());
        StoredBlock sBlock = null;

        try {
            sBlock = blockStore.get(block.getHash());
            if (sBlock.isFullyVerified()) {
                return; // Piece o' cake
            }
            int height = sBlock.getHeight();

            if (block.getTransactions() == null) {
                throw new RuntimeException("verifyTransactions called with Block that didn't have transactions!");
            }

            // All shards have been downloaded, full verification of transactions can happen
            if (scriptVerificationExecutor.isShutdown()) {
                scriptVerificationExecutor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
            }
            List<Future<VerificationException>> listScriptVerificationResults = new ArrayList<Future<VerificationException>>(block.getTransactions().size());

            // Look for douple txID existence (output of tx already in a shard)
            // TODO: we can't fully check for BIP30 violators, what if all the outputs from a tx have been spent
            // but a new tx with the same hash is created after, we're vulnerable to it for the moment
            long sigOps = 0;
            if (!params.isCheckpoint(height)) {
                // BIP30 violator blocks are ones that contain a duplicated transaction. They are all in the
                // checkpoints list and we therefore only check non-checkpoints for duplicated transactions here. See the
                // BIP30 document for more details on this: https://github.com/bitcoin/bips/blob/master/bip-0030.mediawiki
                for (final Transaction tx : block.getTransactions()) {
                    final Set<VerifyFlag> verifyFlags = params.getTransactionVerificationFlags(block, tx, getVersionTally(), height);
                    // If we already have unspent outputs for this hash, we saw the tx already. Either the block is
                    // being added twice (bug) or the block is a BIP30 violator.
                    for (final TransactionOutput output: tx.getOutputs()) {
                        if (blockStore.getTransactionOutput(output.getParentTransactionHash(), output.getIndex()) != null) {
                            throw new VerificationException("Block failed BIP30 test!");
                        }
                        if (verifyFlags.contains(VerifyFlag.P2SH)) {// We already check non-BIP16 sigops in Block.verifyTransactions(true)
                            sigOps += tx.getSigOpCount();
                        }
                    }
                }
            }

            LinkedList<UTXO> utxosSpent = new LinkedList<UTXO>();
            LinkedList<UTXO> utxosCreated = new LinkedList<UTXO>();
            Coin totalFees = Coin.ZERO;
            Coin coinbaseValue = null;

            // Parse every transaction sequentially as they can depend on one another within the same block
            for (final Transaction tx : block.getTransactions()) {
                final List<Script> prevOutScripts = new LinkedList<Script>();
                final Set<VerifyFlag> verifyFlags = params.getTransactionVerificationFlags(block, tx, getVersionTally(), height);
                final boolean isCoinBase = tx.isCoinBase();
                Coin valueIn = Coin.ZERO;
                Coin valueOut = Coin.ZERO;

                // Don't need to verify that the coinbase is a UTXO
                if (!isCoinBase) {
                    // For each input, check it exists in a shard and remove it
                    for (TransactionInput input : tx.getInputs()) {
                        TransactionOutPoint outpoint = input.getOutpoint();
                        UTXO prevOut = blockStore.getTransactionOutput(outpoint.getHash(), outpoint.getIndex());

                        // If input not in store (input never existed or already spent)
                        if (prevOut == null) {
                            throw new VerificationException("Attempted to spend a non-existent or already spent output!");
                        }
                        if (outpoint.getHash().equals(MainNetParams.get().getGenesisBlock().getHash())) {
                            throw new VerificationException("Attempted to spend the genesis block output!");
                        }
                        // Coinbases can only be spent after a certain time (100 blocks in Bitcoin)
                        if (prevOut.isCoinbase() && (height - prevOut.getHeight() < params.getSpendableCoinbaseDepth())) {
                            throw new VerificationException("Tried to spend coinbase at depth " + (height - prevOut.getHeight()));
                        }
                        valueIn = valueIn.add(prevOut.getValue());
                        if (verifyFlags.contains(VerifyFlag.P2SH)) {
                            if (prevOut.getScript().isPayToScriptHash())
                                sigOps += Script.getP2SHSigOpCount(input.getScriptBytes());
                            if (sigOps > Block.MAX_BLOCK_SIGOPS)
                                throw new VerificationException("Too many P2SH SigOps in block");
                        }

                        // Schedule the script verification and remove the now spent UTXO
                        prevOutScripts.add(prevOut.getScript());
                        utxosSpent.add(prevOut);
                        blockStore.removeUnspentTransactionOutput(prevOut, height);
                    }
                }

                // For each output, add it to its respective shard so it can be consumed in future
                for (TransactionOutput output : tx.getOutputs()) {
                    valueOut = valueOut.add(output.getValue());
                    Script script = getScript(output.getScriptBytes());
                    UTXO newUtxo = new UTXO(tx.getHash(),
                            output.getIndex(),
                            output.getValue(),
                            height,
                            isCoinBase,
                            script,
                            getScriptAddress(script));
                    utxosCreated.add(newUtxo);
                    blockStore.addUnspentTransactionOutput(newUtxo, height, block.getHash());
                }

                // Double check the values, transactions with zero output value are OK
                if (valueOut.signum() < 0 || valueOut.compareTo(params.getMaxMoney()) > 0) {
                    throw new VerificationException("Transaction output value out of range");
                }
                if (isCoinBase) {
                    coinbaseValue = valueOut;
                } else {
                    if (valueIn.compareTo(valueOut) < 0 || valueIn.compareTo(params.getMaxMoney()) > 0)
                        throw new VerificationException("Transaction input value out of range");
                    totalFees = totalFees.add(valueIn.subtract(valueOut));
                }

                // Start verifying the tx scripts
                if (!isCoinBase) {
                    // Because correctlySpends modifies transactions, this must come after we are done with tx
                    FutureTask<VerificationException> future = new FutureTask<VerificationException>(new Verifier(tx, prevOutScripts, verifyFlags));
                    scriptVerificationExecutor.execute(future);
                    listScriptVerificationResults.add(future);
                }
            }

            if (totalFees.compareTo(params.getMaxMoney()) > 0 || block.getBlockInflation(height).add(totalFees).compareTo(coinbaseValue) < 0) {
                throw new VerificationException("Transaction fees out of range");
            }

            // Wait for script verification to finish
            for (Future<VerificationException> future : listScriptVerificationResults) {
                VerificationException verifE;
                try {
                    verifE = future.get();
                } catch (InterruptedException thrownE) {
                    throw new RuntimeException(thrownE); // Shouldn't happen
                } catch (ExecutionException thrownE) {
                    log.error("Script.correctlySpends threw a non-normal exception: " + thrownE.getCause());
                    throw new VerificationException("Bug in Script.correctlySpends, likely script malformed in some new and interesting way.", thrownE);
                }
                if (verifE != null)
                    throw verifE;
            }
        } catch (VerificationException e) {
            // FIXME DTC: proper exceptions
            try { notSettingChainHead(); } catch (BlockStoreException e1) {}
            scriptVerificationExecutor.shutdownNow();
            throw new RuntimeException(e);
        } catch (BlockStoreException e) {
            try { notSettingChainHead(); } catch (BlockStoreException e1) {}
            scriptVerificationExecutor.shutdownNow();
            throw new RuntimeException(e);
        }

        // Verifying once is enough
        if (sBlock != null) {
            sBlock.setFullyVerified(true);
        }
    }

    /**
     * Code taken from FullPrunedBlockChain.
     * Get the {@link Script} from the script bytes or return Script of empty byte array.
     */
    private Script getScript(byte[] scriptBytes) {
        try {
            return new Script(scriptBytes);
        } catch (Exception e) {
            return new Script(new byte[0]);
        }
    }

    /**
     * Code taken from FullPrunedBlockChain.
     * Get the address from the {@link Script} if it exists otherwise return empty string "".
     *
     * @param script The script.
     * @return The address.
     */
    private String getScriptAddress(@Nullable Script script) {
        String address = "";
        try {
            if (script != null) {
                address = script.getToAddress(params, true).toString();
            }
        } catch (Exception e) {
        }
        return address;
    }

    /**
     * Code taken from FullPrunedBlockChain.
     */
    private static class Verifier implements Callable<VerificationException> {
        final Transaction tx;
        final List<Script> prevOutScripts;
        final Set<VerifyFlag> verifyFlags;

        public Verifier(final Transaction tx, final List<Script> prevOutScripts, final Set<VerifyFlag> verifyFlags) {
            this.tx = tx;
            this.prevOutScripts = prevOutScripts;
            this.verifyFlags = verifyFlags;
        }

        @Nullable
        @Override
        public VerificationException call() throws Exception {
            try {
                ListIterator<Script> prevOutIt = prevOutScripts.listIterator();
                for (int index = 0; index < tx.getInputs().size(); index++) {
                    tx.getInputs().get(index).getScriptSig().correctlySpends(tx, index, prevOutIt.next(), verifyFlags);
                }
            } catch (VerificationException e) {
                return e;
            }
            return null;
        }
    }
}
