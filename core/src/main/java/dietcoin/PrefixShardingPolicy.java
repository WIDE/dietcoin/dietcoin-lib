package dietcoin;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.BitSet;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.bitcoinj.core.Sha256Hash;
import org.bitcoinj.core.StoredUndoableBlock;
import org.bitcoinj.core.Transaction;
import org.bitcoinj.core.TransactionInput;
import org.bitcoinj.core.UTXO;
import org.bitcoinj.core.Utils;
import org.bitcoinj.store.BlockStoreException;
import org.bitcoinj.store.FullPrunedBlockStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Policy where the first k bits of a transaction hash determines the shard a transaction output belongs to.
 */
public class PrefixShardingPolicy {
    private static final Logger log = LoggerFactory.getLogger(PrefixShardingPolicy.class);

    /** Key length in bits */
    private static int keyLength;
    private static byte[] keyMask;

    /**
     * Apply the sharding policy to the first keyLength bytes of the given hash
     * @param hash A transaction hash
     * @return The shard key this transaction hash belongs to
     */
    public static UTXOShardKey getKeyFor(Sha256Hash hash) {
        byte[] hashBytes = hash.getBytes();
        byte[] result = new byte[Sha256Hash.LENGTH];
        for (int i = 0; i < result.length && i < keyMask.length; i++) {
            result[i] = (byte) (hashBytes[i] & keyMask[i]);
        }
        return UTXOShardKey.wrap(result);
    }

    /**
     * Apply the sharding policy to all the transactions of a block stored in database.
     * @param blockHash The hash of a block stored in the database.
     * @param store A database storing transactions or UTXOs.
     * @return Set of shard keys needed to verify a block, null if not found in the store.
     * @throws BlockStoreException
     */
    @SuppressWarnings("deprecation")
    public static Set<UTXOShardKey> getAllKeysFor(Sha256Hash blockHash, FullPrunedBlockStore store) throws BlockStoreException {
        if (store == null) {
            return null;
        }
        StoredUndoableBlock undoBlock = store.getUndoBlock(blockHash);
        if (undoBlock == null) {
            return null;
        }

        if (undoBlock.getTxOutChanges() == null) {
            // New way of storing txs in database
            return getAllKeysFor(undoBlock.getTransactions());
        } else {
            // Old way of storing txs in database, could it even happen?
            log.warn("Using the old transaction storage format when computing UTXO shard keys.");
            Set<UTXOShardKey> keys = new HashSet<UTXOShardKey>();
            for (UTXO utxo : undoBlock.getTxOutChanges().txOutsSpent) {
                keys.add(PrefixShardingPolicy.getKeyFor(utxo.getHash()));
            }
            for (UTXO utxo : undoBlock.getTxOutChanges().txOutsCreated) {
                keys.add(PrefixShardingPolicy.getKeyFor(utxo.getHash()));
            }
            return keys;
        }
    }

    private static Set<UTXOShardKey> getAllKeysFor(List<Transaction> transactions) {
        if (transactions == null) {
            return null;
        }
        Set<UTXOShardKey> keys = new HashSet<UTXOShardKey>();
        for (Transaction tx : transactions) {
            for (TransactionInput input : tx.getInputs()) {
                if (!input.isCoinBase()) {
                    // Coinbases have one input that must be ignored, it only states it's a coinbase
                    keys.add(PrefixShardingPolicy.getKeyFor(input.getOutpoint().getHash()));
                }
            }
            // For the output(s)
            keys.add(PrefixShardingPolicy.getKeyFor(tx.getHash()));
        }
        return keys;
    }

    /**
     * Generate all the possible keys for the set key length.
     * @return List of all shard keys.
     */
    public static List<UTXOShardKey> generateAllKeys() {
        List<UTXOShardKey> allKeys = new LinkedList<UTXOShardKey>();
        BitSet set = new BitSet(Sha256Hash.LENGTH * 8);
        int pos = 0;
        while (pos < keyLength) {
            // Good looking printing
            // for (int j = 0; j < 40; j++)
            //     System.out.print((set.get(j)) ? "1" : "0");
            // System.out.println();

            allKeys.add(UTXOShardKey.wrap(Arrays.copyOf(set.toByteArray(), Sha256Hash.LENGTH)));

            // Little endian bit adder on a bit set
            pos = 0;
            set.flip(pos);
            while (!set.get(pos)) { // Propagate the addition to a non-zero bit
                pos++;
                set.flip(pos);
            }
        }
        return allKeys;
    }

    public static boolean checkKeyValidity(UTXOShardKey key) {
        if (key.getBytes().length != Sha256Hash.LENGTH)
            return false;
        // TODO DTC: check only the first keyLength bits can be set?
        return true;
    }

    /**
     * Set the number of bits to use as prefix for the policy.
     * @param length Number of bits to use.
     */
    public static void setKeyLengthInBit(int length) {
        keyLength = length;
        BitSet set = new BitSet(Sha256Hash.LENGTH * 8);
        set.set(0, keyLength, true);

        keyMask = new byte[Sha256Hash.LENGTH];
        byte[] setBytes = set.toByteArray();
        System.arraycopy(setBytes, 0, keyMask, 0, setBytes.length);
    }

    public static int getKeyLengthInBit() {
        return keyLength;
    }

    public static void serializeToStream(OutputStream out) throws IOException {
        Utils.uint32ToByteStreamLE(keyLength, out);
    }

    public static void deserializeFromStream(InputStream in) throws IOException {
        setKeyLengthInBit(Utils.readUint32FromByteStreamLE(in));
    }
}
