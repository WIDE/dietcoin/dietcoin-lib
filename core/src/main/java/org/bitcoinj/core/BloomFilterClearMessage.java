package org.bitcoinj.core;

import java.io.IOException;
import java.io.OutputStream;

public class BloomFilterClearMessage extends Message {
    public static int MIN_PROTOCOL_VERSION = 70001;

    @Override
    protected void parse() throws ProtocolException {}

    @Override
    protected void bitcoinSerializeToStream(OutputStream stream) throws IOException {}
}
