package org.bitcoinj.examples;

import java.io.File;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.ArrayList;

import org.bitcoinj.core.Address;
import org.bitcoinj.core.Block;
import org.bitcoinj.core.BlockChain;
import org.bitcoinj.core.Context;
import org.bitcoinj.core.FullPrunedBlockChain;
import org.bitcoinj.core.Message;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.core.Peer;
import org.bitcoinj.core.PeerAddress;
import org.bitcoinj.core.PeerGroup;
import org.bitcoinj.core.StoredBlock;
import org.bitcoinj.core.VerificationException;
import org.bitcoinj.core.listeners.NewBestBlockListener;
import org.bitcoinj.core.listeners.PeerConnectedEventListener;
import org.bitcoinj.core.listeners.PeerDisconnectedEventListener;
import org.bitcoinj.core.listeners.PreMessageReceivedEventListener;
import org.bitcoinj.net.NioServer;
import org.bitcoinj.net.StreamConnection;
import org.bitcoinj.net.StreamConnectionFactory;
import org.bitcoinj.params.MainNetParams;
import org.bitcoinj.store.BlockStore;
import org.bitcoinj.store.BlockStoreException;
import org.bitcoinj.store.FullPrunedBlockStore;
import org.bitcoinj.store.LevelDBBlockStore;
import org.bitcoinj.store.LevelDBFullPrunedBlockStore;
import org.bitcoinj.utils.BriefLogFormatter;
import org.bitcoinj.utils.Threading;
import org.bitcoinj.wallet.DeterministicSeed;
import org.bitcoinj.wallet.Wallet;

public class DietExample {
    public static final String remoteAddress = "FILLME";
    public static final String leveldbNameSrv = "srv_fullpruned";
    public static final int numberBlocksInBaseSrv = 130000;
    public static final String keysFile = "debug_keys.bin";
    public static final String fixedSeed = "dietcoin exp seed";

    public static void init_leveldb_srv() throws Exception {
        NetworkParameters params = MainNetParams.get();
        Context context = Context.getOrCreate(params);
        FullPrunedBlockStore store = new LevelDBFullPrunedBlockStore(params, leveldbNameSrv, Integer.MAX_VALUE);
        FullPrunedBlockChain chain = new FullPrunedBlockChain(context, store);
        PeerGroup group = new PeerGroup(context, chain);
        Wallet wallet = new Wallet(params);

        group.addWallet(wallet);
        group.setConnectTimeoutMillis(10*1000);
        group.disablePeerDiscovery();
        group.setPeerDiscoveryTimeoutMillis(0);
        group.setMaxPeersToDiscoverCount(0);
        group.setMaxConnections(5);

        chain.addNewBestBlockListener(new NewBestBlockListener() {
            @Override
            public void notifyNewBestBlock(StoredBlock block) throws VerificationException {
                if (block.getHeight() % 5000 == 0) {
                    System.out.println("=== new block height: " + block.getHeight());
                }
                if (block.getHeight() >= numberBlocksInBaseSrv) {
                    System.out.println("=== stop at chain size: " + block.getHeight());
                    group.stop();
                    try {
                        store.close();
                    } catch (BlockStoreException e) {
                        e.printStackTrace();
                    }
                    // Otherwise we're stuck with the DownloadProgressTracker waiting for the full chain
                    // to download in group.downloadBlockChain()
                    System.exit(0);
                }
            }
        });
        group.addPreMessageReceivedEventListener(Threading.SAME_THREAD, new PreMessageReceivedEventListener() {
            @Override
            public Message onPreMessageReceived(Peer peer, Message m) {
                if (!(m instanceof Block))
                    System.out.println("--- RECV srv: " + m);
                return m;
            }
        });

        group.start();
        group.connectTo(new InetSocketAddress(remoteAddress, 8333));
        group.waitForPeers(1).get();
        group.downloadBlockChain();
    }

    public static void init_keys() throws Exception {
        NetworkParameters params = MainNetParams.get();
        Wallet wallet = Wallet.fromSeed(params, new DeterministicSeed(fixedSeed.getBytes(), new ArrayList<>(), 0));
        System.out.println(wallet.getWatchingKey().toString());

        // These addresses have tx linked to them in the firsts 130k blocks, use blockchain.info to see them
        wallet.addWatchedAddress(Address.fromBase58(params, "1zXzDGhCaQKwwswB8WzqEvtyg6AbcZBTM"), 0);
        wallet.addWatchedAddress(Address.fromBase58(params, "1KKQHhasw82cjLidcrEFmcXavtXcja3qdH"), 0);
        wallet.saveToFile(new File(keysFile));
        System.out.println("Wrote keys to file:" + keysFile);

        // TODO DTC: debug deterministic bloom filter and receiving tx where the key sends money
//        Wallet wallet2 = Wallet.loadFromFile(new File(keysFile));
//        System.out.println("BLOOM2: " + wallet2.getBloomFilter(0.05, 0L));
//        System.out.println("BLOOM: " + wallet.getBloomFilter(0.05, 0L));
//        System.out.println("BLOOM: " + wallet.getBloomFilter(0.05, 0L));
    }

    public static void main(String[] args) throws Exception {
        if ("FILLME".equals(remoteAddress)) {
            System.out.println("The class variable remoteAddress must point to a bitcoin full node. Exiting.");
            return;
        }
        BriefLogFormatter.init();

        /**
         *  Used to create the server storage for tests
         */
        init_leveldb_srv();
        System.out.println("Proxy leveldb initialized, you can use it to test your client now");
        if (true) return;

        /**
         * Init the watched keys
         */
        init_keys();
        System.out.println("Key initialized, you can use it to test your client now");
        if (true) return;

        /**
         * server
         */
        NetworkParameters paramsSrv = MainNetParams.get();
        Context contextSrv = Context.getOrCreate(paramsSrv);
        FullPrunedBlockStore storeSrv = new LevelDBFullPrunedBlockStore(paramsSrv, leveldbNameSrv, Integer.MAX_VALUE);
        FullPrunedBlockChain chainSrv = new FullPrunedBlockChain(contextSrv, storeSrv);

        NioServer server = new NioServer(new StreamConnectionFactory() {
            Peer remoteClient = null;

            @Override
            public StreamConnection getNewConnection(InetAddress inetAddress, int port) {
                remoteClient = new Peer(paramsSrv, chainSrv, new PeerAddress(inetAddress, port), "DTC server", "0.1", true);
                remoteClient.addConnectedEventListener(Threading.SAME_THREAD, new PeerConnectedEventListener() {
                    @Override
                    public void onPeerConnected(Peer peer, int peerCount) {
                        System.out.println("--- srv: connection from " + peer);
                    }
                });
                remoteClient.addDisconnectedEventListener(Threading.SAME_THREAD, new PeerDisconnectedEventListener() {
                    @Override
                    public void onPeerDisconnected(Peer peer, int peerCount) {
                        System.out.println("--- srv: disconnection from " + peer);
                    }
                });
//                remoteClient.addPreMessageReceivedEventListener(Threading.SAME_THREAD, new PreMessageReceivedEventListener() {
//                    @Override
//                    public Message onPreMessageReceived(Peer peer, Message m) {
////                        System.out.println("--- srv: received msg from " + peer + ": " + m);
//                        System.out.println("=== SEND msg: " + m);
//                        return m;
//                    }
//                });
                return remoteClient;
            }
        }, new InetSocketAddress("127.0.0.1", 4243));
        server.startAsync();
        server.awaitRunning();


        /**
         * client
         */
        NetworkParameters params = MainNetParams.get();
        Context context = Context.getOrCreate(params);
        BlockStore store = new LevelDBBlockStore(context, new File("cli_header"));
        BlockChain chain = new BlockChain(context, store);
//        FullPrunedBlockStore store = new LevelDBFullPrunedBlockStore(params, "cli_fullpruned", Integer.MAX_VALUE);
//        FullPrunedBlockChain chain = new FullPrunedBlockChain(context, store);
        PeerGroup group = new PeerGroup(context, chain);
        Wallet wallet = Wallet.loadFromFile(new File(keysFile));

        group.addWallet(wallet);
        if (chain.getClass().equals(BlockChain.class) || chain.getClass().equals(DietBlockChain.class)) {
            // For SPV, prevent downloading filtered blocks before this catchup time
            group.setFastCatchupTimeSecs(0);
        }
        group.setConnectTimeoutMillis(10*1000);
        group.disablePeerDiscovery();
        group.setPeerDiscoveryTimeoutMillis(0);
        group.setMaxPeersToDiscoverCount(0);
        group.setMaxConnections(5);
        group.setUserAgent("DTC client", "0.1");

        group.addPreMessageReceivedEventListener(Threading.SAME_THREAD, new PreMessageReceivedEventListener() {
            @Override
            public Message onPreMessageReceived(Peer peer, Message m) {
                System.out.println("=== RECV cli: " + m);
                return m;
            }
        });

        chain.addNewBestBlockListener(new NewBestBlockListener() {
            @Override
            public void notifyNewBestBlock(StoredBlock block) throws VerificationException {
                if (block.getHeight() % 5000 == 0) {
                    System.out.println("=== new block height: " + block.getHeight());
                }
                if (block.getHeight() >= numberBlocksInBaseSrv) {
                    System.out.println("=== stop at chain size: " + block.getHeight());
                    group.stop();
                    try {
                        store.close();
                    } catch (BlockStoreException e) {
                        e.printStackTrace();
                    }
                    // Otherwise we're stuck with the DownloadProgressTracker waiting for the full chain
                    // to download in group.downloadBlockChain()
                    System.exit(0);
                }
            }
        });


        group.start();
        Peer remoteSrv = group.connectTo(new InetSocketAddress("127.0.0.1", 4243));
//        Peer remoteSrv = group.connectTo(new InetSocketAddress(remoteAddress, 8333));
        System.out.println("--- cli: remote server is: " + remoteSrv);

        System.out.println("*** sleeping 1s");
        Thread.sleep(1000);

        System.out.println("--- cli: starting download");
        group.downloadBlockChain();

        group.stop();
        store.close();

        server.stopAsync();
        server.awaitTerminated();
        storeSrv.close();
    }
}
